\section{Vectors}

Vector types are a whole family of types used mainly to store data. These types
are called \code{REALSXP}, \code{INTSXP}, \code{LGLSXP}, \code{CPLXSXP}, \code{STRSXP}, \code{RAWSXP},
\code{VECSXP}, and \code{EXPRSXP}. Most of the vector types represent vectors available
to the R programmer: \code{character}, \code{numeric}, \code{logical}, etc.  The one exception
is a type of vector used internally to store strings called \code{CHARSXP}.

Vector types are actually a little uncharacteristic, since they don't use the
SEXP structure, and instead need to be cast to a type called \code{VECSEXP} (do not
confuse with \code{VECSXP}, the name of the type) if we want to make sense of their
internals. A \code{VECSEXP} is a pointer to a structure called \code{VECTOR\_SEXPREC}
defined as follows:

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]
\sexpHeader{(0,0)}{};
\nextSlot{length}			{R\_xlen\_t}{length}{}		{};
\nextSlot{truelength}	    {R\_xlen\_t}{truelength}{}	{};
\nextAlign{align}		     						{};
\nextDataOmitted{data}	 {DATAPTR}        					{44+nB};
\sexpRange{sxpinfo}{gengcn}{SEXPREC\_HEADER};
\sexpRange{length}{truelength}{VECTOR\_SEXPREC};
\sexpRangeBottom{length}{align}{SEXPREC\_ALIGN};
\end{tikzpicture}
\caption{\label{fig:langsxp-example} \code{VECTOR\_SEXPREC} anatomy.}
\end{figure}

A \code{VECTOR\_SEXPREC} also has a standard SEXP header structure first, but then it has 
payload of type \code{vecsxp\_struct}. The payload contains two pieces of information: the
length of the vector and its true length. 

The length represents how many elements the vector currently holds. Apparently,
vector length used to be limited to $2^{31}-1$, but currently the maximum
length can be up to $2^{64}-1$. So, currently \code{R\_xlen\_t} is just an alias for
\code{int}. The length of a vector can be retrieved using the \code{XLENGTH} macro.

True length is mostly unused. It is used in when hash tables are implemented
using \code{VECSXP}s to indicate the number of primary slots in use, and for the
reference hash tables in serialization, where truelength is the number of slots
in use.

That data itself is located in a block of bytes that follows \code{vecsxp} in
memory. The data is aligned as required, so there may be an artificial gap
between the end of the \code{VECSEXP} structure and the beginning of data. There is a
convenience structure to help us find the beginning of the data called
\code{SEXPREC\_ALIGN} defined as follows, which aligns the structure above with a
\code{double}:

\begin{example}
typedef union { VECTOR_SEXPREC s; double align; } SEXPREC_ALIGN;
\end{example}

We can then get a pointer to the data as follows:

\code{}`C
\begin{example}
((SEXPREC_ALIGN *) (x)) + 1
\end{example}

We should cast the pointer into whatever data type the vector contains, or in
the general case:

\code{}`C
\begin{example}
((void *) ((SEXPREC_ALIGN *) (x)) + 1)
\end{example}

This is encapsulated in the macro \code{DATAPTR}.

Now onto specific sub-types of vectors!

\begin{table}
{\centering\scriptsize
\begin{tabular}{lllll}
\toprule
type           & constructor (in R)               & coercion (in R)                        & contains        & descriptiom                      \\ \midrule
\code{REALSXP} & \code{numeric(N)}                & \code{as.numeric(V)}                   & \code{double}   & vector of reals/floating points  \\
\code{INTSXP}  & \code{integer(N)}                & \code{as.integer(V)}                   & \code{int}      & vector of integers               \\
\code{LGLSXP}  & \code{logical(N)}                & \code{as.logical(V)}                   & \code{Rboolean} & vector of logical/boolean values \\
\code{CPLXSXP} & \code{complex(N)}                & \code{as.complex(V)}                   & \code{Rcomplex} & vector of complex numbers        \\
\code{STRSXP}  & \code{character(N)}              & \code{as.character(V)}                 & \code{CHARSXP } & vector of character strings      \\
\code{RAWSXP}  & \code{raw(N)}                    & \code{as.raw(V)}                       & \code{Rbyte }   & vector of raw bytes              \\
\code{VECSXP}  & \code{vector(mode="list")}       & \code{as.vector(mode="list", V)}       & \code{SEXP}     & generic vector of SEXPs          \\
\code{EXPRSXP} & \code{vector(mode="expression")} &  \code{as.vector(mode="expression, V)} & \code{SEXP}     & vector of expressions (LANGSXPs) \\
\bottomrule
\end{tabular}   
}\caption{\label{lab:vectors} Vectory summary. \code{N}: initial length, \code{V}: another vector.}
\end{table}

We explain the type \code{CHARSXP further} below, and the types \code{Rcomplex} and \code{Rboolean}
are defined as follows:

\begin{example}
typedef char                           Rbyte;
typedef enum { FALSE = 0, TRUE }       Rboolean;
typedef struct { double r; double i; } Rcomplex;
\end{example}

Let's take a look at an example vector:

\begin{example}
c(1, 2, 3)
\end{example}

We could represent this vector as follows (without the header): \ref{fig:realsxp-example}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

\sexpHeaderOmitted{(0,0)}{header};
\nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 3}				{};
\nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{blue!50} 0}			{};
\nextSlot{real0}		{double}{[0]}{\color{green!50} 1.0}		        	{};
\nextSlot{real1}		{double}{[1]}{\color{green!50} 2.0}		        	{};
\nextSlot{real2}		{double}{[2]}{\color{green!50} 3.0}		        	{68B};
\sexpLabelRight{lab}{27,6}{1.5cm}{length}{XLENGTH(~)};
\sexpLabelRight{lab}{27,5}{1.5cm}{truelength}{XTRUELENGTH(~)};
\sexpLabelRight{lab}{27,4}{1.5cm}{real0}{REAL(~)};
\sexpLabelRight{lab}{27,3}{1.5cm}{real0}{REAL\_RO(~)};
\end{tikzpicture}
\caption{\label{fig:realsxp-example} \code{REALSXP}.}
\end{figure}


On the C side of things, the block of bytes containing the elements are
accessible through the \code{DATAPTR} macro. The results have to be coerced into the
appropriate types, and there are macros that do this too:

\begin{example}
#define LOGICAL(x)      ((int *)            DATAPTR(x))
#define INTEGER(x)      ((int *)            DATAPTR(x))
#define RAW(x)          ((Rbyte *)          DATAPTR(x))
#define COMPLEX(x)      ((Rcomplex *)       DATAPTR(x))
#define REAL(x)         ((double *)         DATAPTR(x))
\end{example}

We can also cast the data into read-only types:

\begin{example}
#define LOGICAL_RO(x)   ((const int *)      DATAPTR_RO(x))
#define INTEGER_RO(x)   ((const int *)      DATAPTR_RO(x))
#define RAW_RO(x)       ((const Rbyte *)    DATAPTR_RO(x))
#define COMPLEX_RO(x)   ((const Rcomplex *) DATAPTR_RO(x))
#define REAL_RO(x)      ((const double *)   DATAPTR_RO(x))
\end{example}

These return an array of elements that we can traverse as follows:

\begin{example}
SEXP s;
for (int i = 0; i < XLENGTH(s); i++) {
    INTEGER(s)[i] += 1;
}
\end{example}

There are also macros and functions for reading or writing just one element of
the vector: \code{VECTOR\_ELT} and \code{SET\_VECTOR\_ELT} for the general case, and then
\code{LOGICAL\_ELT} and \code{SET\_LOGICAL\_ELT}, \code{INTEGER\_ELT} and \code{SET\_INTEGER\_ELT},
etc.  for specific sub-types.

\begin{example}
SEXP s;
for (int i = 0; i < XLENGTH(s); i++) {
    SET_INTEGER_ELT(s, i, INTEGER_ELT(s, i) + 1);
}
\end{example}

These are implemented by coercing the results of \code{DATAPTR} into appropriate
types.

\subsection{Scalars}


One of the flags in SEXP headers is the \code{scalar} flag. This flag is used to
indicate that a numeric (or logical) vector is of length one. So:

\begin{example}
> v1 <- 1       # scalar flag = 1
> v2 <- c(1)    # scalar flag = 1
> v3 <- c(1,1)  # scalar flag = 0
\end{example}

There is a macro that allows us to access the scalar flag from C which checks
whether a particular SEXP is a scalar vector of a particular type.

\begin{example}
#define IS_SCALAR(x, t) (((x)->sxpinfo.type == (t)) && (x)->sxpinfo.scalar)
\end{example}

The knowledge about whether a vector is scalar is used by the interpreter for
optimizations. For instance, the \code{do\_arith} function that, as the name
suggests, does arithmetic, will perform a much cheaper addition, say, right
there and then, rather than firing up the machinery to add two vectors
together.


\subsection{Internal character vectors}

Vectors of type \code{CHARSXP} are used internally by other SEXPs to store character
strings. For example, they are used to store the elements of \code{STRSXP}, as well as
the printable names of a \code{SYMSXP}s. They can only be seen as parts of other
SEXPs and are transparent to programmers.

We can retrieve the contents of a \code{CHARSXP} via the \code{CHAR} macro, which returns a
C string.

The idea of encoding should be fairly self-explanatory, but what is \code{cached}?
By default R maintains a (hashed) global cache of \code{variables} (that is symbols
and their bindings) which have been found, and this refers only to environments
which have been marked to participate, which consists of the global environment
(aka the user workspace), the base environment plus environments which have
been attached to the search path (see \code{attach} function). When an environment
is either attached or detached, the names of its symbols are flushed from the
cache. The cache is used whenever searching for variables from the global
environment (possibly as part of a recursive search).

The encoding and caching are specified in the general purpose bits of the
header. There are bit masks defined for specific encodings and a bit mask for
the cached flag:

\begin{example}
#define BYTES_MASK  (1<<1)
#define LATIN1_MASK (1<<2)
#define UTF8_MASK   (1<<3)
                             /* (1<<4) is taken by S4_OBJECT_MASK */
#define CACHED_MASK (1<<5)
#define ASCII_MASK  (1<<6)
\end{example}

They can be checked and set by using macros like \code{IS\_UTF8} and \code{SET\_UTF8} which
compare or set the appropriate \code{gp} bits against specific masks, e.g.:

\begin{example}
# define IS_UTF8(x)  ((x)->sxpinfo.gp & UTF8_MASK)
# define SET_UTF8(x) (((x)->sxpinfo.gp) |= UTF8_MASK)
\end{example}

\subsection{Expression vectors}

Expression vectors are similar to generic vectors, except that there is an
expectation that a character vector contains \code{LANGSXP}s (although is not enforced
by the SEXP itself).

\subsection{Alternative representation}

The vectors also have an *alternative representation* (*altrep*). This is a new
inclusion into R (3.5.0) meant to speed up the implementation and clean up the
interface. Currently, altrep vectors are created in very limited cases (sequences) and I
don't have a lot of experience with them. For these reasons and given that this
adventure in writing is already a lot longer than I initially planned I will
leave altrep for later. In the meantime there are a number of sources out there
introducing altrep and otherwise discussing it. \footnote{
Sources of information about altrep:

    \begin{itemize}
    \item https://gmbecker.github.io/jsm2017/jsm2017.html
    \item https://www.r-project.org/dsc/2017/slides/dsc2017.pdf
    \item https://homepage.divms.uiowa.edu/\~luke/talks/nzsa-2017.pdf
    \item http://blog.revolutionanalytics.com/2017/09/altrep-preview.html
    \end{itemize}
}

I will only mention that there is a header flag \code{alt} which is set to \code{1} if
the vector uses altrep and \code{0} otherwise. This flag can be looked up and
modified via the following macro:

\begin{example}
#define ALTREP(x)       ((x)->sxpinfo.alt)
#define SETALTREP(x, v) (((x)->sxpinfo.alt) = (v))
\end{example}
