\section{Promises}

When arguments are passed to functions the argument expression is usually
wrapped in a promise. This gives R its lazy semantics. Despite being created
whenever most functions are called, promises are generally invisible to R
programmers, and some do not even know they exist. Structure-wise promises are
specified by SEXPs of type `PROMSXP` and their payload is described by
`promsxp\_struct` accessible via the `promsxp` field of the payload union. The
`promsxp\_struct` structure has three slots: `value`, `expr`, and `env`:
\ref{fig:promsxp}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{value}	{SEXP}{value}{}     {};
    \nextSlot{expr}	    {SEXP}{expr}{}	    {};
    \nextSlot{env}	    {ENVSXP}{env}{}	    {52B};

    %\sexpRange{sxpinfo}{gengcn}{SEXPREC\_HEADER};
    \sexpRangeBottom{value}{env}{promsxp\_struct};

    \sexpLabelRight{lab}{17,4}{1.5cm}{expr}{PRCODE(~)};
    \sexpLabelRight{lab}{17,5}{1.5cm}{value}{PRVALUE(~)};
    \sexpLabelRight{lab}{17,3}{1.5cm}{env}{PRENV(~)};
\end{tikzpicture}
\caption{\label{fig:promsxp} PROMSXP.}
\end{figure}

In the typical case, when a promise is created, it wraps some expression passed
to a function. The promise retains the original unevaluated expression in the
`expr` slot. It also retains the environment in `env` which should be used to
evaluate the expression in `expr`. In the case of expressions passed to some
function, `env` will be the enclosing environment of that function. In the case
of expressions defined as default arguments, `env` will be the environment
defined within the function. The `value` slot initially points to the
`R\_UnboundValue` symbol. The following example shows an example of a promise at
creation:\ref{fig:newpromsxp}

\begin{figure}
\centering
\begin{minipage}{.48\linewidth}
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{value}	{SYMSXP}{value}{\color{red} R\_Unbound...}     {};
    \nextSlot{expr}	    {LANGSXP}{expr}{\color{green!30} 2 + 2}	       {};
    \nextSlot{env}	    {ENVSXP}{env}{\color{green!30} R\_GlobalEnv}   {52B};

\end{tikzpicture}
\caption{\label{fig:newpromsxp} New promise.}
\end{minipage}
\begin{minipage}{.48\linewidth}
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{value}	{REALSXP}{value}{\color{green!30} 4.0}   {};
    \nextSlot{expr}	    {LANGSXP}{expr}{\color{green!30} 2 + 2}	 {};
    \nextSlot{env}	    {ENVSXP}{env}{\color{red} R\_NilValue}   {52B};

\end{tikzpicture}
\caption{\label{fig:forcedpromsxp} Forced promise.}
\end{minipage}
\end{figure}


Whenever the interpreter requires the value of a promise, it performs a check
to see whether the promise has previously been seen by checking if its `value`
points to `R\_UnboundValue`.  If that is the case, the promise is *forced*. This
means its `expr` is evaluated using the environment pointed to by `env` and the
result of the evaluation is stored in `value`. For the duration of forcing the
youngest bit of the `gp` field in the header is set (accessible by the
`PRSEEN`/`SET\_PRSEEN` macros), to detect and prevent recursion. After the
promise finishes evaluating the expression, `env` is set to `R\_NilValue`.

Forcing the example above will yield the following: \ref{fig:forcedpromsxp}

A promise force is triggered whenever the argument holding the promise is
assigned or returned from the function. Promises can also be forced as a
result of being passed to another function: always in the case of a built-in.
When a promise is passed to a special function, it depends on the function
whether it will evaluate the promise or not. In the case of a being passed to a
closure it may be the case that the promise is wrapped in another promise and
then forced whenever the wrapper promise get forced (although it is my
understanding that these cases are being eliminated by R core devs in the
current version). 


Below is an example illustrating these cases

\begin{example}
f <- function(a, b, c, d, e) { 
    tmp <- a + 1                  # forces promise bound to `a`
    print(b)                      # forces promise bound to `b` before entering print
    c                             # forces promise bound to `c`
    g(d)                          # forces promise bound to `d` inside `g` 
}                                 # promise bound to `e` is not forced
g <- function(x) {
    x                             # forces promise bound to `x`: this will force any nested promises inside `x`
}
\end{example}

Apart from the typical case we also see promises that are created pre-forced.
In that case, the `value` is set to something else than `R\_UnboundValue` from
the outset, and other fields may or may not be forced as required by the use
case.

\subsection{Dot-dot-dot}

Since we have all those breakpoints set up, there is one specific type of SEXP
which is a list, but we really only see it in conjunction with promises, and
that's `DOTSXP`.  

To inspect it, let's make a function that takes variable arguments and returns
them via a vector, then let's run that function with some arguments:

\begin{example}
f <- function(...) c(...)
f(x=1, y=2, 3)
\end{example}

At this point let us look at the formal parameters of our function:
We see that the formals are a single-element list containing a pointer to a
symbol describing our variable argument list. This symbol is actually the
predefined singleton `R\_DotsSymbol`, by the way. We can get the values passed
as arguments by looking up the bindings of symbols from formals in the
function's environment `newrho`. We can do this using `findVar`, although one
should always be careful using that function, since it can potentially have
side effects. So, let us instead do it the old-fashioned way, by inspecting the
environment.

\begin{example}
(gdb) p R_inspect(newrho)
@55555950e3c8 04 ENVSXP g0c0 [gp=0x1000] <0x55555950e3c8>
FRAME:
  @55555950e2e8 02 LISTSXP g0c0 [] 
    TAG: @555555c1b7b0 01 SYMSXP g1c0 [MARK,NAM(3)] "..."
    @55555950e390 17 DOTSXP g0c0 [] 
ENCLOS:
  @555555c4ef70 04 ENVSXP g1c0 [MARK,NAM(3),GL,gp=0x8000] <R_GlobalEnv>
$15 = (struct SEXPREC *) 0x55555950e3c8
\end{example}

And finally, we have come across a `DOTSXP` list, which is helpfully rendered
by the inspector with no information whatsoever:

Well, it is a sub-type of `LISTSXP` that contains as `carval` promises that wrap
the arguments passed via `...` and as `tagval` (optionally) symbols that were
used to pass them. For our example the structure is the following boring list: \ref{fig:dotdotdot}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    %\sexpHeaderOmitted{(0,0)}{header};
    %\nextSlot{value}	{SEXP}{value}{}     {};
    %\nextSlot{expr}	    {SEXP}{expr}{}	    {};
    %\nextSlot{env}	    {ENVSXP}{env}{}	    {52B};

    \sexpHeaderOmitted{(0,0)}{header2};
    \nextSlot{car2}	{PROMSXP}{carval}{\color{green!30} 1.0}	        {};
    \nextSlot{cdr2}	{LISTSXP}{cdrval}{\color{green!30} \textbullet}	{};
    \nextSlot{tag2}	{SYMSXP}{tagval}{\color{green!30} x}  {52B};

    \sexpHeaderOmitted{(0,-6)}{header3};
    \nextSlot{car3}	{PROMSXP}{carval}{\color{green!30} 2.0}	        {};
    \nextSlot{cdr3}	{LISTSXP}{cdrval}{\color{green!30} \textbullet}  {};
    \nextSlot{tag3}	{SYMSXP}{tagval}{\color{green!30} y}  {52B};

    \sexpPointerDown{cdr2}{header3}{2.2cm};
    
    \sexpHeaderOmitted{(0,-12)}{header4};
    \nextSlot{car4}	{PROMSXP}{carval}{\color{green!30} 3.0}	        {};
    \nextSlot{cdr4}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag4}	{SYMSXP}{tagval}{~}  {52B};

    \sexpPointerDown{cdr3}{header4}{1.5cm};

\end{tikzpicture}
\caption{\label{fig:dotdotdot} Dot-dot-dot.}
\end{figure}

\subsection{Promises in the wild}

Promises are the second largest group of SEXPs wee see. One universal fact about promises we see in the data is that they do not have attributes (all attributes are set to `R\_NilValue`).

\subsubsection{Expressions contained within promises}

A breakdown according to the `expr` field. It shows what expression was
executed or was supposed to be executed by the promise.

\begin{example}
       expr jit.count jit.percent nojit.count nojit.percent
   BCODESXP     5.65B      56.25%     137.58M         0.99%
    LANGSXP     1.24B      12.38%        3.2B        23.13%
     LGLSXP     1.15B      11.45%       2.13B        15.38%
     STRSXP   684.53M        6.8%       1.31B         9.49%
     SYMSXP   440.57M       4.38%       5.38B        38.83%
    REALSXP   265.76M       2.64%     349.34M         2.51%
     INTSXP   223.91M       2.22%     503.92M         3.63%
 R_NilValue   212.46M       2.11%     461.35M         3.32%
    PROMSXP     60.5M        0.6%     254.03M         1.83%
     VECSXP     60.5M        0.6%      64.35M         0.46%
     CLOSXP    25.38M       0.25%       25.3M         0.18%
     ENVSXP    22.32M       0.22%      22.82M         0.16%
      S4SXP     4.32M       0.04%       4.48M         0.03%
  EXTPTRSXP   144.74K         <1%      145.4K           <1%
    LISTSXP   114.38K         <1%     157.17K           <1%
 BUILTINSXP    74.33K         <1%      74.33K           <1%
     RAWSXP    20.76K         <1%      20.76K           <1%
    EXPRSXP       340         <1%         340           <1%
    CPLXSXP        91         <1%         105           <1%
     DOTSXP         1         <1%           1           <1%
\end{example}

Note that there are very few byte code SEXPs in the NOJIT version, meaning that
while they are being created, very few of them are passed around.

In JIT we see BCODESXPs make up around 56\% of promises, uncompiled function
applications make up ~12\%, vectors make up ~23\% (mostly booleans and
strings), symbols make up ~4\%, and NILs make up ~2\%. 

In NOJIT we see a quite different distribution, ~39\% are symbols, ~31\% are
vectors, ~23\% are function applications, and ~3\% are NILs. There is still a
small but significant number of BCODESXP that get passed around despite the
bytecode being disabled, which is bizarre.

\subsubsection{Return values within promises}

A breakdown according to the `value` field. This field shows what the promise
returned after it was evaluated.


\begin{example}
          value jit.count jit.percent nojit.count nojit.percent
         STRSXP     2.43B      24.16%       3.58B        25.86%
        REALSXP     1.64B      16.36%       2.06B         14.9%
 R_UnboundValue     1.31B      13.05%       1.35B         9.77%
         LGLSXP     1.23B      12.29%       2.25B        16.26%
         ENVSXP   915.79M        9.1%     934.11M         6.73%
         INTSXP   847.21M       8.42%       1.31B         9.48%
         VECSXP   685.33M       6.81%     959.98M         6.92%
         CLOSXP   405.42M       4.03%     421.39M         3.03%
     R_NilValue   298.04M       2.96%      670.3M         4.83%
        LANGSXP   105.19M       1.04%      108.4M         0.78%
          S4SXP     71.1M        0.7%      72.96M         0.52%
     BUILTINSXP    46.77M       0.46%      46.84M         0.33%
         SYMSXP    41.94M       0.41%      49.65M         0.35%
      EXTPTRSXP     5.26M       0.05%       6.77M         0.04%
     SPECIALSXP     2.78M       0.02%       2.97M         0.02%
        EXPRSXP     1.73M       0.01%       1.76M         0.01%
        LISTSXP     1.56M       0.01%       2.89M         0.02%
        CPLXSXP   829.27K         <1%       7.49M         0.05%
       BCODESXP   275.86K         <1%     159.26K           <1%
         RAWSXP   248.28K         <1%      372.3K           <1%
     WEAKREFSXP     53.1K         <1%     146.23K           <1%
         DOTSXP      1.3K         <1%       2.61K           <1%
        PROMSXP      <NA>        <NA>          14           <1%
\end{example}

In ~68\% of the promises, they return a vector of one type, or another.
Otherwise, in ~13\% of the cases we get a `R\_UnboundValue` symbol, indicating
that the promise was not evaluated yet. Finally, in around 9\% of the cases the
promise returns an environment. It's quite a large chunk of them that return
environments, in fact. Around 3\% we return a NIL, and around 1\% we get some
code for a function application that needs evaluation.

\subsubsection{Promise environments and a quick look at laziness}

A breakdown according to the `env` field. We're looking at promises at the end
of their life, so we can determine how many of them were evaluated and how many
were not. We can do this by observing the `env` field in (in conjunction with
`value`. Let's just look at `env` first.

\begin{example}
        env jit.count jit.percent nojit.count nojit.percent
 R_NilValue     8.42B      83.76%      12.19B        87.95%
     ENVSXP     1.63B      16.23%       1.67B        12.04%
\end{example}

The `env` field is either an environment or a NIL. If it's a NIL, this is
typically supposed to signify that the promise was already evaluated: upon
evaluation the macro changes the environment to NIL. It also sets the `value`
field to the result returned by the expression. 

The ~1\\6\% number is higher than the ~13\% number
of \code{R\_UnboundValue}s. This is what we see when we check the specifics:

\begin{example}
          value    env jit.count jit.percent nojit.count nojit.percent
 R_UnboundValue ENVSXP     1.31B      13.05%       1.35B         9.77%
         VECSXP ENVSXP    238.6M       2.37%     233.02M         1.67%
        REALSXP ENVSXP    30.19M        0.3%      30.84M         0.22%
         INTSXP ENVSXP     23.6M       0.23%      23.32M         0.16%
          S4SXP ENVSXP    11.76M       0.11%       11.8M         0.08%
         STRSXP ENVSXP     5.05M       0.05%       5.06M         0.03%
         ENVSXP ENVSXP     4.74M       0.04%       4.74M         0.03%
        LANGSXP ENVSXP     2.26M       0.02%       2.56M         0.01%
         LGLSXP ENVSXP     2.13M       0.02%        2.1M         0.01%
         CLOSXP ENVSXP     1.19M       0.01%       1.19M           <1%
        LISTSXP ENVSXP    57.51K         <1%      57.51K           <1%
     R_NilValue ENVSXP    21.59K         <1%      21.59K           <1%
         RAWSXP ENVSXP    20.19K         <1%       19.9K           <1%
      EXTPTRSXP ENVSXP     5.73K         <1%       5.73K           <1%
        CPLXSXP ENVSXP       133         <1%         133           <1%
     SPECIALSXP ENVSXP         3         <1%           3           <1%
\end{example}

I cannot currently explain the promises which have an ENVSXP and a value. They
are likely to be pre-evaluated promises with an empty environment, but this
needs checking. The promise evaluation macro will only execute the promise if
its value is equal to `R\_UnboundValue`.

For now I'll limit anything I say to just the promises that start with
`R\_UnboundValue` in `env` and `ENVSXP` in `env`---clear cases of unevaluated
promises. Or, of course, the clear cases of evaluated promises where `env` is
`R\_NilValue` and `value` is set to something other than `R\_UnboundValue`.

Unevaluated promises make up only about 13\% of promises within the dataset.
The unevaluated promises break down as follows:

\begin{example}
          value    env       expr jit.count jit.percent nojit.count nojit.percent
 R_UnboundValue ENVSXP    LANGSXP   475.82M       4.73%     593.74M         4.28%
 R_UnboundValue ENVSXP     LGLSXP   281.49M       2.79%     298.12M         2.14%
 R_UnboundValue ENVSXP   BCODESXP   244.86M       2.43%       3.36M         0.02%
 R_UnboundValue ENVSXP     STRSXP   157.24M       1.56%     164.98M         1.18%
 R_UnboundValue ENVSXP     SYMSXP     69.4M       0.69%     209.99M         1.51%
 R_UnboundValue ENVSXP R_NilValue    41.49M       0.41%      42.06M          0.3%
 R_UnboundValue ENVSXP    REALSXP    19.23M       0.19%       21.4M         0.15%
 R_UnboundValue ENVSXP    PROMSXP     17.3M       0.17%      16.22M         0.11%
 R_UnboundValue ENVSXP     INTSXP      4.9M       0.04%       5.21M         0.03%
 R_UnboundValue ENVSXP     VECSXP     1.21M       0.01%       1.21M           <1%
 R_UnboundValue ENVSXP     ENVSXP    12.47K         <1%      12.51K           <1%
 R_UnboundValue ENVSXP     CLOSXP     3.89K         <1%       3.87K           <1%
 R_UnboundValue ENVSXP      S4SXP     1.38K         <1%       1.38K           <1%
 R_UnboundValue ENVSXP     RAWSXP       276         <1%         276           <1%
 R_UnboundValue ENVSXP    LISTSXP        11         <1%          11           <1%
 R_UnboundValue ENVSXP    EXPRSXP         4         <1%           4           <1%
 R_UnboundValue ENVSXP     DOTSXP         1         <1%           1           <1%
\end{example}

What we see most of here is unevaluated function applications, followed
significantly by booleans, byte code, strings, symbols and nils. 

Evaluated promises make make up the remaining 84\% of promises. They break down
like this in terms of expressions inside:

\begin{example}
        env       expr jit.count jit.percent nojit.count nojit.percent
 R_NilValue   BCODESXP     5.41B      53.82%     134.16M         0.96%
 R_NilValue     LGLSXP   869.13M       8.64%       1.83B        13.22%
 R_NilValue    LANGSXP   754.41M        7.5%       2.61B        18.82%
 R_NilValue     STRSXP   523.54M        5.2%       1.14B         8.27%
 R_NilValue    REALSXP   225.14M       2.23%     305.91M          2.2%
 R_NilValue     INTSXP   208.46M       2.07%     488.28M         3.52%
 R_NilValue     SYMSXP   189.04M       1.88%       5.16B        37.24%
 R_NilValue R_NilValue   170.95M        1.7%     419.27M         3.02%
 R_NilValue     CLOSXP    25.37M       0.25%      25.29M         0.18%
 R_NilValue     ENVSXP    22.29M       0.22%      22.79M         0.16%
 R_NilValue    PROMSXP    16.86M       0.16%       36.2M         0.26%
 R_NilValue     VECSXP     4.58M       0.04%       7.06M         0.05%
 R_NilValue  EXTPTRSXP    140.2K         <1%     140.85K           <1%
 R_NilValue    LISTSXP   114.37K         <1%     157.16K           <1%
 R_NilValue      S4SXP   112.35K         <1%     260.53K           <1%
 R_NilValue BUILTINSXP    74.33K         <1%      74.33K           <1%
 R_NilValue     RAWSXP    11.16K         <1%      11.16K           <1%
 R_NilValue    EXPRSXP       336         <1%         336           <1%
 R_NilValue    CPLXSXP        10         <1%          24           <1%
\end{example}

We see the results dominated by byte code expressions at ~54\%. About 18\% are
various vectors. Then, we have function applications at ~7.5\%, symbols at
~2\%, and NILs at less than 2\%. NOJIT changes a lot, since BCODESXP is very
small, and other categories, like symbols are relatively larger.

I also wanted to see breakdowns of what percentages of `expr` types are
evaluated or not. Here evaluated is intuitively either `TRUE` or `FALSE` or it
is NA if it falls into that category of promises with an environment but also
with a value other than `R\_UnboundValue`. I grouped the results into groups.

\paragraph{Almost exclusively eager (99\%+)}

\begin{example}
       expr evaluated jit.count jit.percent nojit.count nojit.percent

 BUILTINSXP      TRUE    74.33K        100%      74.33K          100%

     ENVSXP     FALSE    12.47K       0.05%      12.51K         0.05%
     ENVSXP      TRUE    22.29M      99.88%      22.79M        99.88%
     ENVSXP        NA    13.25K       0.05%      13.25K         0.05%

    LISTSXP     FALSE        11         <1%          11           <1%
    LISTSXP      TRUE   114.37K      99.99%     157.16K        99.99%

     CLOSXP     FALSE     3.89K       0.01%       3.87K         0.01%
     CLOSXP      TRUE    25.37M      99.98%      25.29M        99.98%
     CLOSXP        NA       529         <1%         529           <1%
\end{example}

\paragraph{Vast majority eager (95\%+)}

\begin{example}
       expr evaluated jit.count jit.percent nojit.count nojit.percent

    EXPRSXP     FALSE         4       1.17%           4         1.17%
    EXPRSXP      TRUE       336      98.82%         336        98.82%

      S4SXP     FALSE     1.38K       0.03%       1.38K         0.03%
      S4SXP      TRUE   112.35K       2.59%     260.53K          5.8%
      S4SXP        NA     4.21M      97.37%       4.22M        94.16%

  EXTPTRSXP      TRUE    140.2K      96.86%     140.85K        96.87%
  EXTPTRSXP        NA     4.54K       3.13%       4.54K         3.12%

   BCODESXP     FALSE   244.86M       4.32%       3.36M         2.44%
   BCODESXP      TRUE     5.41B      95.67%     134.16M        97.51%
   BCODESXP        NA    59.05K         <1%      51.95K         0.03%

     DOTSXP     FALSE         1        100%           1          100%
\end{example}

\paragraph{Majority eager (80\%+)}

\begin{example}
       expr evaluated jit.count jit.percent nojit.count nojit.percent

     INTSXP     FALSE      4.9M       2.19%       5.21M         1.03%
     INTSXP      TRUE   208.46M      93.09%     488.28M        96.89%
     INTSXP        NA    10.54M       4.71%      10.42M         2.06%

     VECSXP     FALSE     1.21M       2.01%       1.21M         1.89%
     VECSXP      TRUE     4.58M       7.57%       7.06M        10.98%
     VECSXP        NA     54.7M      90.41%      56.06M        87.12%

    CPLXSXP      TRUE        10      10.98%          24        22.85%
    CPLXSXP        NA        81      89.01%          81        77.14%

    REALSXP     FALSE    19.23M       7.23%       21.4M         6.12%
    REALSXP      TRUE   225.14M      84.71%     305.91M        87.56%
    REALSXP        NA    21.39M       8.04%      22.02M          6.3%

 R_NilValue     FALSE    41.49M      19.53%      42.06M         9.11%
 R_NilValue      TRUE   170.95M      80.46%     419.27M        90.87%
 R_NilValue        NA    20.72K         <1%      20.72K           <1%
\end{example}

\paragraph{Fairly lazy}

\begin{example}
       expr evaluated jit.count jit.percent nojit.count nojit.percent

     STRSXP     FALSE   157.24M      22.97%     164.98M        12.53%
     STRSXP      TRUE   523.54M      76.48%       1.14B        87.18%
     STRSXP        NA     3.74M       0.54%       3.74M         0.28%

     LGLSXP     FALSE   281.49M      24.43%     298.12M        13.96%
     LGLSXP      TRUE   869.13M      75.43%       1.83B        85.95%
     LGLSXP        NA     1.54M       0.13%       1.51M         0.07%

    LANGSXP     FALSE   475.82M      38.21%     593.74M        18.49%
    LANGSXP      TRUE   754.41M      60.58%       2.61B        81.34%
    LANGSXP        NA    14.95M        1.2%       4.98M         0.15%

     RAWSXP     FALSE       276       1.32%         276         1.32%
     RAWSXP      TRUE    11.16K      53.76%      11.16K        53.76%
     RAWSXP        NA     9.32K       44.9%       9.32K         44.9%

     SYMSXP     FALSE     69.4M      15.75%     209.99M         3.89%
     SYMSXP      TRUE   189.04M       42.9%       5.16B        95.91%
     SYMSXP        NA   182.12M      41.33%      10.05M         0.18%

    PROMSXP     FALSE     17.3M       28.6%      16.22M         6.38%
    PROMSXP      TRUE    16.86M      27.87%       36.2M        14.25%
    PROMSXP        NA    26.33M      43.52%     201.61M        79.36%
\end{example}


