\section{The anatomy of a SEXP}

From the point of view of the internal C code of the R interpreter, a SEXP is a
pointer to a struct called \samp{SEXPREC}. This structure consists of a header
and a payload. We investigate both below. We provide an idealized
representation of a SEXP in the diagram in Fig.~\ref{fig:anatomy}.

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeader{(0,5)}{};
    \nextSlot{car}	{SEXP}{}{}	{};
    \nextSlot{cdr}	{SEXP}{}{}	{};
    \nextSlot{tag}	{SEXP}{}{}	{52B};

    \sexpRange{sxpinfo}{gengcn}{SEXPREC\_HEADER};
    \sexpRangeBottom{car}{tag}{u};
\end{tikzpicture}
\caption{\label{fig:anatomy} Anatomy of a SEXP.}
\end{figure}

\subsection{Header}

Let’s start by looking at the header. And let’s start from the end: \code{slots
gengc\_prev\_node} and \code{gen\_gc\_next\_node} are used by the garbage
collector to iterate over the SEXPs in the same generation, they point other
SEXPs. All the SEXPs form a circular doubly-linked list. This has specific
perks for node removal. \footnote{The source code sends us to The Treadmill: Real-Time Garbage Collection
   Without Motion Sickness by Henry G. Baker
   (http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.23.5878\&rep=rep1\&type=pdf).} The specifics of the inner workings of the GC are out
of scope for this paper.

Then attrib is also another SEXP that describes add-on attributes of the SEXP
we’re analyzing, usually used for metadata such as class or source reference
information. Attributes are used to hang information on an SEXP without
interfering with its contents. We describe them in detail in a separate section
further below.

Finally, the sxpinfo part of the header is a structure defined like this:

\begin{example}
#define NAMED_BITS 16
struct sxpinfo_struct {
    SEXPTYPE type      :  5;
    unsigned int scalar:  1; 
    unsigned int obj   :  1;
    unsigned int alt   :  1;
    unsigned int gp    : 16;
    unsigned int mark  :  1;
    unsigned int debug :  1;
    unsigned int trace :  1;
    unsigned int spare :  1;
    unsigned int gcgen :  1;
    unsigned int gccls :  3;
    unsigned int named : NAMED_BITS;
    unsigned int extra : 32 - NAMED_BITS;
}; /*           Tot: 64 */
\end{example}

We’ll ignore most of these for now, with just a few exceptions. For now we will
go through the flags which often appear on the print outs of our inspection
tool. Most of the other flags pertain to specific SEXP types, so we will
explain them as they come up. All the other flags we will discuss at the very
end.

\subsubsection{Type}

The \code{type} member is very important and it informs us what kind of SEXP we're
dealing with and how to extract useful information from its payload. This
member is of type \code{SEXPTYPE}, which is basically an \code{unsigned int}. For every
type there is a corresponding constant that's a little more hamster-readable.
Here are the types and their constants as defined in \code{Rinternals.h}:


% TODO maybe make a table?
\begin{example}
typedef unsigned int SEXPTYPE;

#define NILSXP         0     /* null */
#define SYMSXP         1     /* symbols */
#define LISTSXP        2     /* lists (specifically: pairlists) */
#define CLOSXP         3     /* closures */
#define ENVSXP         4     /* environments */
#define PROMSXP        5     /* promises */
#define LANGSXP        6     /* language constructs (special lists) */
#define SPECIALSXP     7     /* special functions */
#define BUILTINSXP     8     /* builtin non-special functions */
#define CHARSXP        9     /* internal string type*/
#define LGLSXP        10     /* logical vectors */

#define INTSXP        13     /* integer vectors */
#define REALSXP       14     /* real variables */
#define CPLXSXP       15     /* complex variables */
#define STRSXP        16     /* strings/character vectors */
#define DOTSXP        17     /* dot-dot-dot object */
#define ANYSXP        18     /* make "any" args work. */
#define VECSXP        19     /* generic vectors */
#define EXPRSXP       20     /* expression vectors */
#define BCODESXP      21     /* byte code */
#define EXTPTRSXP     22     /* external pointer */
#define WEAKREFSXP    23     /* weak reference */
#define RAWSXP        24     /* raw byte vector */
#define S4SXP         25     /* S4 classes, non-vector */

#define NEWSXP        30     /* fresh node created in new page */
#define FREESXP       31     /* node released by GC */
#define FUNSXP        99     /* closure or builtin or special */
\end{example}

Most SEXP type constants refer to specific concrete types of SEXPs: symbols,
integer vectors, etc. The type of an SEXP informs us what are the semantics of
the three payload slots in \code{u} and gives us a hint about which macros to use to
access and modify the SEXP. We'll tackle these types one by one below in depth
as we talk about the payload.

Meanwhile there are some idiosyncrasies here worth noting. First, constants 11
and 12 are left free. These used to be used for internal factors and ordered
factors. They are no longer part of the language. Nevertheless, the gap should
not be re-used, since the ordering of SEXP types matter.

The types \code{ANYSXP} and \code{FUNSXP} are internally by the interpreter as aggregated
types for convenience. \code{FUNSXP} is used to indicate either a SEXP of type
\code{CLOSXP}, \code{BUILTINSXP}, or \code{SPECIALSXP}. \code{ANYSXP} is used to indicate a SEXP of
any type.  This does not mean that there are SEXPs of types \code{FUNSXP} or
\code{ANYSXP} floating around. Rather these are used in various conditions in the
interpreter (see e.g. function \code{findVar1} from \code{envir.c}).

There is a macro in C that retrieves the type of a SEXP as a \code{SEXPTYPE} called
\code{TYPEOF} which can be readily used with the type constants to make conditions.

\begin{example}
SEXP s;
if (TYPEOF(s) == NILSXP) {
// ...
}
\end{example}

\subsubsection{Named}

The \code{named} field is less significant in our discussion, but it appears in the
inspector quite often, so it deserves a description. This field is used to
indicate how many times this object has been assigned to a variable (how many
times it has been named).

Let us say we have a vector of integers. If the vector has not been assigned to
any variable its named field is 0. This indicates there are not references to
this vector.

If we assign the vector to a variable and inspect that, we see that \code{named} is
set to \code{1}, indicating that there is exactly one reference to the vector. If we
then assign \code{x} to another variable \code{y}, \code{named} will be set to \code{2}, again,
indicating that there are two references to \code{named}. Note that \code{named} is
increased also in \code{x} after the assignment:

However, if we continue the assignment, \code{named} plateaus at a number specified
by the constant \code{NAMEDMAX} defined in \code{Rinternals.h}. In my interpreter that
number is set to \code{3}. Thus \code{named} set to 3 indicates that there are 3 or more
references to this vector. If we create more references to this vector, the
\code{named} field continues to be set to \code{3}.

The purpose of counting the references is to provide an illusion of *copy by
value* semantics in R while preventing the interpreter from needlessly
performing costly copying operations. When a SEXP is about to be altered, the
interpreter checks the \code{named} field. Values within the range of \code{2} and
\code{NAMEDMAX} signify that the SEXP must be duplicated before the alteration is
applied. A value of \code{0} means that it is known that no other SEXP shares data
with this one, and so it may safely be altered in-place. A value of \code{1}
signifies that there are two copies of the SEXP in principle, but one of them
exists only for the duration of computation, so many operations can avoid
copying in this case. There are handy macros that illustrate the semantics of
\code{named}:

\begin{example}
#define MAYBE_SHARED(x) (NAMED(x) > 1)
#define NO_REFERENCES(x) (NAMED(x) == 0)
#define MARK_NOT_MUTABLE(x) SET_NAMED(x, NAMEDMAX)
#define MAYBE_REFERENCED(x) (! NO_REFERENCES(x))
#define NOT_SHARED(x) (! MAYBE_SHARED(x))
\end{example}

There are two macros to look up and manipulate the \code{named} field directly:

\begin{example}
#define SET_NAMED(x,v)  (((x)->sxpinfo.named)=(v))
#define NAMED(x)        ((x)->sxpinfo.named)
\end{example}

\subsubsection{General purpose}

There are 16 general-purpose bits used by various specific SEXPs to indicate
type-specific information. Generally speaking there are a number of bit masks
that are used within the interpreter to see whether a particular bit is set or
not. For instance:

\begin{example}
#define MISSING_MASK	15
#define MISSING(x)	((x)->sxpinfo.gp & MISSING_MASK)
\end{example}

We will point out specific masks and their uses as we discuss specific SEXPs.

\subsubsection{GC}

There are a few flags which pertain to the GC which will make recurring
appearances. These are: \code{mark}, \code{gcgen}, and \code{gccls}. R has a generational
garbage collector. 

The \code{mark} flag is used by the GC to mark which objects are currently in use.
The value of \code{1} signifies that the object is in use and \code{0} signifies that
it is not.

The \code{gcgen} specifies which generation of old objects this SEXP belongs to. This
can either be \code{0} or \code{1}.

Finally, \code{gccls} specifies the class of objects to which this object belongs.
There are up to 8 classes (\code{0}--\code{7}) among which there is a special class for
large nodes, nodes with custom initializers, and multiple classes for small
nodes. 

\subsection{Payload}

Apart from the header the SEXP contains the following union \code{u} which contains
the meat of the SEXP:

\begin{example}
union {
    struct primsxp_struct primsxp;
    struct symsxp_struct  symsxp;
    struct listsxp_struct listsxp;
    struct envsxp_struct  envsxp;
    struct closxp_struct  closxp;
    struct promsxp_struct promsxp;
} u;
\end{example}

The component of the union on which to operate is chosen depending on the type:
\code{closxp} will have members that make sense for function definitions, \code{promsxp}
will have members representing a promise, etc. There's rarely a need to look
into this union directly either, since there are ample macros that retrieve
particular members of particular SEXP types. 

Let's break down how to use the payload by SEXP type.

