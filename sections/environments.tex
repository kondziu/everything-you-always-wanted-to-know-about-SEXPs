\section{Environments}

Environments are structures that store bindings from variables (\code{SYMSXP}s) to
values. They are expressed by \code{ENVSXP}s. Their payload can be accessed by the
\code{envsxp} field of the payload union, which is specified by \code{envsxp\_struct}. \ref{fig:envsxp}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{frame}	{LISTSXP}{frame}{}			    {};
    \nextSlot{enclos}	{ENVSXP}{enclos}{}	            {};
    \nextSlot{hashtab}	{VECSXP}{hashtab}{}		        {52B};

    %\sexpRange{sxpinfo}{gengcn}{SEXPREC\_HEADER};
    \sexpRangeBottom{car}{tag}{envsxp\_struct};

    \sexpLabelRight{lab}{17,5}{1.5cm}{frame}{FRAME(~)};
    \sexpLabelRight{lab}{17,4}{1.5cm}{enclos}{ENCLOS(~)};
    \sexpLabelRight{lab}{17,3}{1.5cm}{hashtab}{HASHTAB(~)};
\end{tikzpicture}
\caption{\label{fig:envsxp} ENVSXP.}
\end{figure}

Environments have three slots: \code{frame}, \code{enclos}, and \code{hashtab}. The \code{enclos}
slot is used to construct hierarchies of environments. The \code{frame} and
\code{hashtab} slots are used to store bindings between variables and their values:
hash table environments use the \code{hashtab} slot and list environemnts use
\code{frame}. We will go through how \code{enclos} works first, and then describe hash
table and list evironments in detail.

\paragraph{Environments}

Environments form tree-like hierarchies. This is accomplished by the \code{enclos}
slot which points to the enclosing (parent) environment. This is how lexical
scoping is implemented in R: every function creates its own environment, but
these environments for a hierachy with the environments of enclosing functions.
Specifically, when a function is evaluated it creates a new environment and
sets its \code{enclos} slot to the enclosing environment of the function. if Let us
look at a brief example of this:

\begin{example}
x <- 1

f1 <- function() f2()
f2 <- function() {f3(); f3()}
f3 <- function() x + 1

f1()
\end{example}

We define a variable \code{x} and assign the value of \code{1} to it, and then define
three functions: \code{f1}, \code{f2}, \code{f3}. Since the variable \code{x} is defined in the top
level, it is defined in the global environment called \code{R\_GlobalEnv}. Next we
execute function \code{f1}. Then \code{f1} will create some environment which we will
denote \code{e1}. The \code{f1} function is a top level function, and top level functions
use \code{R\_GlobalEnv} as their enclosing environment. So \code{e1}'s enclosing
environment is set to \code{R\_GlobalEnv}. Then, \code{f1} executes \code{f2}. When \code{f2} starts
executing, it creates an environment which we will denote \code{e2}, and its
enclosing environment is \code{e1}. Then, \code{f2} executes \code{f3} twice in succession.
Each time \code{f3} creates a new environment (denoted \code{e3a} and \code{e3b}) whose
enclosing environment is \code{e2}. This produces the following environment tree:


\begin{example}
\code{R_GlobalEnv}
 |-e1
   |-e2
     |-e3a
     |-e3b
\end{example}

When eventually \code{f3} looks up \code{x} it tries to look it up in \code{f3}'s environment,
so \code{e3a}. Since \code{x} is not defined there the lookup algorithm proceeds to look
\code{x} up in \code{e3a}'s enclosing environment \code{e2}. Variable \code{x} is not defined there
either, so the lookup proceeds to the next enclosing environment, etc. until it
reaches \code{R\_GlobalEnv}. There, \code{x} is defined, so the lookup algorithm returns
the value assigned to \code{x}.

R defines several special environments. We saw \code{R\_GlobalEnv} which represents
the global environment, used as the enclosing environment for top level
functions and therefore as the root of the environment hierarchy created by
function applications. There is also an empty environment defined to be used as
roots of environment hierarchies. The empty environment is a singleton
accessible under the global variable \code{R\_EmptyEnv}. \code{R\_EmptyEnv} is defined so
that its \code{frame}, \code{enclos}, and \code{hashtab} are all set to \code{R\_NilValue}. Finally,
there is \code{R\_BaseEnv}, a base environment. \code{R\_GlobalEnv}'s enclosing environment
is \code{R\_BaseEnv}, whose enclosing environment is \code{R\_EmptyEnv}.

\begin{example}
\code{R_NilValue}
 |-\code{R_EmptyEnv}
    |-\code{R_BaseEnv}
       |-\code{R_GlobalEnv}
\end{example}

The slots of the environments can be accessed by the following macros:

\begin{example}
#define FRAME(x)	((x)->u.envsxp.frame)
#define ENCLOS(x)	((x)->u.envsxp.enclos)
#define HASHTAB(x)	((x)->u.envsxp.hashtab)
\end{example}

There are two basic types of environments. 

\paragraph{Hash table environments}

Usually environments store bindings in a hash table. An environment's hash
table is implemented using a vector in the \code{hashtab} slot. In that case the
\code{frame} slot points to \code{R\_NilValue}.

The \code{VECSXP} implementing the hash table is a generic vector containing SEXPs.
The vector uses \code{length} to indicate the allocated size of the hash table and
\code{truelength} as the number of elements that are currently in use. An element
that is not in use points to a \code{NILSXP}. An element that is in use points to a
pairlist (\code{LISTSXP}) containing bindings.

Before looking at how exactly bindings are stored in the vector let us first
take a look at how hashes are calculated. Every binding contains a symbol and a
value.  The symbol is a \code{SYMSXP}, for example the following: \ref{fig:binding}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{car}	{LISTSXP}{pname}{\color{green!30} "x"}			    {};
    \nextSlot{cdr}	{ENVSXP}{value}{\color{red} R\_NilValue}           {};
    \nextSlot{tag}	{VECSXP}{internal}{\color{red} R\_NilValue}          {52B};

\end{tikzpicture}
\caption{\label{fig:binding} Example of a binding symbol.}
\end{figure}

A symbol's hash is calculated using the
\code{CHARSXP} that is the symbol's \code{pname}. The \code{CHARSXP} is rendered as a C
character string and then the hashing is performed on that string using the
algorithm from the Dragon Book:

\begin{example}
int attribute_hidden R_Newhashpjw(const char *s)
{
    char *p;
    unsigned h = 0, g;
    for (p = (char *) s; *p; p++) {
	h = (h << 4) + (*p);
	if ((g = h & 0xf0000000) != 0) {
	    h = h ^ (g >> 24);
	    h = h ^ g;
	}
    }
    return h;
}
\end{example}

\code{R\_Newhashpjw} yields an integer hash and stores it with the symbol's \code{pname}.
Specficially the hash is stored as the \code{truelength} of the \code{CHARSXP} that is
the symbol's \code{pname}. This is shown in the example in Fig.~\ref{fig:hashstorage}. In addition, the
\code{HASHASH\_FLAG} is set in the \code{gp} field of the SEXP's header.


\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,7)}{header};
    \nextSlot{car}	{LISTSXP}{pname}{\color{green!30} "x"}			    {};
    \nextSlot{cdr}	{ENVSXP}{value}{\color{red} R\_NilValue}           {};
    \nextSlot{tag}	{VECSXP}{internal}{\color{red} R\_NilValue}          {52B};

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{pname}	{LISTSXP}{pname}{\color{green!30} \textbullet}	{};
    \nextSlot{enclos}	{ENVSXP}{enclos}{\color{red} R\_NilValue}	    {};
    \nextSlot{hashtab}	{VECSXP}{hashtab}{\color{red} R\_NilValue}      {52B};

    \sexpHeaderOmitted{(0,-7)}{header1};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 1}				{};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{blue!50} 121}		{};
    \nextHalf{int0}		    {int}{[0]}{\color{green!50} 'x'}		        	{48B};

    \sexpNoteRight{lab}{17,-4}{1.5cm}{truelength}{hash (from \code{R\_Newhashpjw})};

    \sexpPointerDown{pname}{header1}{1.5cm};
    \sexpPointerDown{car}{header}{1.5cm};

\end{tikzpicture}
\caption{\label{fig:hashstorage} Storing hashes in bindings.}
\end{figure}

In order to place a binding of some value to some symbol, we create a new  SEXP
of type \code{LISTSXP} whose \code{tagval} will point to the SEXP representing the symbol
and whose \code{carval} will point to the SEXP representing the value. For instance:
\ref{fig:bindingplacement}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{car}	{REALSXP}{carval}{\color{green!30} 1.0}	    {};
    \nextSlot{cdr}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	{};
    \nextSlot{tag}	{SYMSXP}{tagval}{\color{green!30} x}        {52B};

\end{tikzpicture}
\caption{\label{fig:bindingplacement} Binding placement.}
\end{figure}

This binding will become a part of a chain of bindings stored as one of the
elements of the \code{hashtab} vector. The position in the vector is determined by a
\code{hashcode} which we calculate by modulo dividing the \code{hash} of the symbol in
the binding by the length of \code{hashtab}. By default \code{hashtab} length is 29
(resized by a factor of 1.2 whenever \code{truelength} grows to be 85\% or more of
\code{length}).

So, for the example above, the \code{hashcode} is \code{121 \% 29 = 4}. This means that in
a 0-indexed vector, we would index the vector with 4 to get the chain that's
appropriate for this binding. 
The appropriate chain is therefore located at index 5 (\code{VECTOR\_ELT(table, 5)}).

The position in \code{hashtab} where we want to insert our binding will point to
some existing chain which will be either a \code{NILSXP} or a \code{LISTSXP}. Our
binding will be prepended to the existing chain: the position in \code{hashtab}
will now point to our binding, and our binding's \code{cdrval} will point to the old
chain.

Let's go through an example. First let's create an environment. We use
\code{new.env} to do that. If we don't specify any parameters, we will get a hash
table environment with the initial size of 29. The enclosing environment is
going to be the global environment: \code{R\_GlobalEnv}.

\begin{example}
new.env()      # defaults: hash=TRUE, size=29
\end{example}

This structure can be illustrated as follows: \ref{fig:newenv}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{frame}	{NILSXP}{frame}{\color{red} R\_NilValue}	    {};
    \nextSlot{enclos}	{ENVSXP}{enclos}{\color{green!30} R\_GlobalEnv}	{};
    \nextSlot{hashtab}	{VECSXP}{hashtab}{\color{green!30} \textbullet} {52B};

    \sexpHeaderOmitted{(0,-7)}{header1};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 29}    {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		{};
    \nextSlot{e0}		    {NILSXP}{[0]}{\color{red} R\_NilValue}      {};
    \nextSlot{e1}		    {NILSXP}{[1]}{\color{red} R\_NilValue}      {};
    \nextSlot{e2}		    {NILSXP}{[2]}{\color{red} R\_NilValue}      {};
    \nextDataOmittedMiddle  {omitted}{\ldots}                           {200}{};
    \nextSlot{e0}		    {NILSXP}{[28]}{\color{red} R\_NilValue}     {276B};

    \sexpPointerDown{hashtab}{header1}{1.5cm};
\end{tikzpicture}
\caption{\label{fig:newenv} New empty environment.}
\end{figure}

We subsequently add two bindings to the environment:

\begin{example}
e$x <- 1 
e$y <- 2
\end{example}

The hashes of \code{x} and \code{y} respectively are \code{120} and \code{121}, which for a
29-element hash table will produce hash codes of \code{120 \% 29 = 4} and `121 \% 29
= 5\code{. This means that }x\code{ and }y` will be placed in chains located at positions
4 and 5 in the \code{hashtab} vector: \ref{fig:nolongernewenv}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{frame}	{NILSXP}{frame}{\color{red} R\_NilValue}	    {};
    \nextSlot{enclos}	{ENVSXP}{enclos}{\color{green!30} R\_GlobalEnv}	{};
    \nextSlot{hashtab}	{VECSXP}{hashtab}{\color{green!30} \textbullet} {52B};

    \sexpHeaderOmitted{(0,-7)}{header1};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 29}			{};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{blue!50} 2}   		{};
    \nextSlot{e0}		    {NILSXP}{[0]}{\color{red} R\_NilValue}          	{};
    \nextDataOmittedMiddle  {omitted}{\ldots}                                   {32}{};
    \nextSlot{e5}		    {LISTSXP}{[5]}{\color{green!30} \textbullet}       	{};
    \nextSlot{e6}		    {LISTSXP}{[6]}{\color{green!30} \textbullet}       	{};
    \nextDataOmittedMiddle  {omitted}{\ldots}                                   {168}{};
    \nextSlot{e0}		    {NILSXP}{[28]}{\color{red} R\_NilValue}          	{276B};

    \sexpPointerDown{hashtab}{header1}{1.5cm};

    \sexpHeaderOmitted{(0,-15)}{header2};
    \nextSlot{car}	{REALSXP}{carval}{\color{green!30} 1.0}	        {};
    \nextSlot{cdr}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag}	{SYMSXP}{tagval}{\color{green!30} x}  {52B};

    \sexpPointerDown{e5}{header2}{1.5cm};

    \sexpHeaderOmitted{(20,-15)}{header3};
    \nextSlot{car}	{REALSXP}{carval}{\color{green!30} 2.0}	        {};
    \nextSlot{cdr}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag}	{SYMSXP}{tagval}{\color{green!30} y}  {52B};

    \sexpPointerDown{e6}{header3}{2.2cm};
\end{tikzpicture}
\caption{\label{fig:nolongernewenv} Bindings in environments.}
\end{figure}

Finally, let's add one more binding for \code{dt}: \ref{fig:chainbindings}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{frame}	{NILSXP}{frame}{\color{red} R\_NilValue}	    {};
    \nextSlot{enclos}	{ENVSXP}{enclos}{\color{green!30} R\_GlobalEnv}	{};
    \nextSlot{hashtab}	{VECSXP}{hashtab}{\color{green!30} \textbullet} {52B};

    \sexpHeaderOmitted{(0,-7)}{header1};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 29}			{};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{blue!50} 3}   		{};
    \nextSlot{e0}		    {NILSXP}{[0]}{\color{red} R\_NilValue}          	{};
    \nextDataOmittedMiddle  {omitted}{\ldots}                                   {32}{};
    \nextSlot{e5}		    {LISTSXP}{[5]}{\color{green!30} \textbullet}       	{};
    \nextSlot{e6}		    {LISTSXP}{[6]}{\color{green!30} \textbullet}       	{};
    \nextDataOmittedMiddle  {omitted}{\ldots}                                   {168}{};
    \nextSlot{e0}		    {NILSXP}{[28]}{\color{red} R\_NilValue}          	{276B};

    \sexpPointerDown{hashtab}{header1}{1.5cm};

    \sexpHeaderOmitted{(0,-15)}{header2};
    \nextSlot{car2}	{REALSXP}{carval}{\color{green!30} 1.0}	        {};
    \nextSlot{cdr2}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag2}	{SYMSXP}{tagval}{\color{green!30} x}  {52B};

    \sexpPointerDown{e5}{header2}{1.5cm};

    \sexpHeaderOmitted{(20,-15)}{header3};
    \nextSlot{car3}	{REALSXP}{carval}{\color{green!30} 3.0}	        {};
    \nextSlot{cdr3}	{NILSXP}{cdrval}{\color{green!30} \textbullet}  {};
    \nextSlot{tag3}	{SYMSXP}{tagval}{\color{green!30} dt}  {52B};

    \sexpPointerDown{e6}{header3}{2.2cm};
    
    \sexpHeaderOmitted{(20,-22)}{header4};
    \nextSlot{car4}	{REALSXP}{carval}{\color{green!30} 2.0}	        {};
    \nextSlot{cdr4}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag4}	{SYMSXP}{tagval}{\color{green!30} y}  {52B};

    \sexpPointerDown{cdr3}{header4}{1.5cm};
\end{tikzpicture}
\caption{\label{fig:chainbindings} Chain bindings in environemnts.}
\end{figure}

Note that \code{truelength} increased to 3.

Some useful macros for hash table environments. The following can be used to
retrieve the \code{hashtab} vector and information about it:

\begin{example}
#define IS_HASHED(x)            (HASHTAB(x) != R_NilValue)
#define HASHTAB(x)              ((x)->u.envsxp.hashtab)
#define HASHSIZE(x)             ((int) STDVEC_LENGTH(x))
#define HASHPRI(x)              ((int) STDVEC_TRUELENGTH(x))
#define HASHTABLEGROWTHRATE     1.2
#define HASHMINSIZE             29
\end{example}

These macros can be used to retrieve chains and bindings from a \code{hashtab}
(including cases of active bindings which are out of scope for this
already lengthy discussion):

\begin{example}
#define HASHCHAIN(table, i)     ((SEXP *) STDVEC_DATAPTR(table))[i]
#define BINDING_VALUE(b)        ((IS_ACTIVE_BINDING(b) ? getActiveValue(CAR(b)) : CAR(b)))
#define SYMBOL_BINDING_VALUE(s) ((IS_ACTIVE_BINDING(s) ? getActiveValue(SYMVALUE(s)) : SYMVALUE(s)))
#define SYMBOL_HAS_BINDING(s)   (IS_ACTIVE_BINDING(s) || (SYMVALUE(s) != R_UnboundValue))
\end{example}

Finally, these macros can be used to retrieve and manipulate hashes of
individual SEXPs:

\begin{example}
#define HASHASH_MASK 1
#define HASHASH(x)         ((x)->sxpinfo.gp & HASHASH_MASK)
#define HASHVALUE(x)       ((int) TRUELENGTH(x))
#define SET_HASHASH(x,v)   ((v) ? (((x)->sxpinfo.gp) |= HASHASH_MASK) : (((x)->sxpinfo.gp) &= (~HASHASH_MASK)))
#define SET_HASHVALUE(x,v) SET_TRUELENGTH(x, ((int) (v)))
\end{example}

% e <- new.env()      # defaults: hash=TRUE, size=29
% e$x  <- 1           # hash of \code{x}  is  120 % 29 = 4 -> lives at VECTOR_ELT(5)
% e$ai <- 2           # hash of \code{ai} is 1657 % 29 = 4 -> lives at VECTOR_ELT(5)
% e$y  <- 2           # hash of \code{y}  is  121 % 29 = 5 -> lives at VECTOR_ELT(6)
% e$dt <- 3           # hash of \code{dt} is 1716 % 29 = 5 -> lives at VECTOR_ELT(6)

\paragraph{List environments}

List environments are environments that use internal lists for storing bindings
rather than hash tables. Such environments will have their \code{hashtab} slot
pointing to \code{R\_NilValue} and their \code{frame} slot pointing to either
a \code{R\_NilValue} (signifying an empty environment) or a pairlist (\code{LISTSXP}). Such
a pairlist would contain symbols under \code{tagval}and values under \code{carval}.

List environments are fairly straightforward. We'll follow a simple example of
creating and populating one:

\begin{example}
new.env(hash=FALSE)
\end{example}

This creates an empty environment with both \code{hashtab} and \code{frame} pointing to
\code{R\_NilValue}: \ref{fig:listenvironment}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{frame}	{NILSXP}{frame}{\color{red} R\_NilValue}    {};
    \nextSlot{enclos}	{ENVSXP}{enclos}{\color{green!30} R\_GlobalEnv}	            {};
    \nextSlot{hashtab}	{NILSXP}{hashtab}{\color{red} R\_NilValue}  {52B};
\end{tikzpicture}
\caption{\label{fig:listenvironment} New list environment.}
\end{figure}

We subsequently add a binding to the environment:

\begin{example}
e$x <- 1
\end{example}

As with hash table environments, in order to place a binding of some value to
some symbol, we create a new SEXP of type \code{LISTSXP} whose \code{tagval} will point
to the SEXP representing the symbol and whose \code{carval} will point to the SEXP
representing the value. Our binding will be prepended to the existing \code{frame}:
\code{frame} will now point to our binding, and our binding's \code{cdrval} will point to
the old \code{frame}: \ref{fig:listenvironmentbinding}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{frame}	{NILSXP}{frame}{\color{red} R\_NilValue}    {};
    \nextSlot{enclos}	{ENVSXP}{enclos}{\color{green!30} R\_GlobalEnv}	            {};
    \nextSlot{hashtab}	{NILSXP}{hashtab}{\color{red} R\_NilValue}  {52B};

    \sexpHeaderOmitted{(0,-6)}{header1};
    \nextSlot{car1}	{REALSXP}{carval}{\color{green!30} 1.0}	        {};
    \nextSlot{cdr1}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag1}	{SYMSXP}{tagval}{\color{green!30} x}  {52B};

    \sexpPointerDown{frame}{header1}{1.5cm};

\end{tikzpicture}
\caption{\label{fig:listenvironmentbinding} List environment with a binding.}
\end{figure}

If we then add another binding:

\begin{example}
e$y <- 2
\end{example}

That binding will also be prepended to \code{frame}: \ref{fig:listenvironmentbindings}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{frame}	{NILSXP}{frame}{\color{red} R\_NilValue}    {};
    \nextSlot{enclos}	{ENVSXP}{enclos}{\color{green!30} R\_GlobalEnv}	            {};
    \nextSlot{hashtab}	{NILSXP}{hashtab}{\color{red} R\_NilValue}  {52B};

    \sexpHeaderOmitted{(0,-6)}{header1};
    \nextSlot{car1}	{REALSXP}{carval}{\color{green!30}2.0}	        {};
    \nextSlot{cdr1}	{LISTSXP}{cdrval}{\color{green!30} \textbullet}	    {};
    \nextSlot{tag1}	{SYMSXP}{tagval}{\color{green!30} y}            {52B};

    \sexpPointerDown{frame}{header1}{1.5cm};

    \sexpHeaderOmitted{(0,-12)}{header2};
    \nextSlot{car2}	{REALSXP}{carval}{\color{green!30} 1.0}	        {};
    \nextSlot{cdr2}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag2}	{SYMSXP}{tagval}{\color{green!30} x}            {52B};

    \sexpPointerDown{cdr1}{header2}{1.5cm};
\end{tikzpicture}
\caption{\label{fig:listenvironmentbindings} List environment with two bindings.}
\end{figure}


\subsection{Environments in the wild}

A summary of environments:

\begin{example}
     attrib      frame    hashtab     enclos jit.count jit.percent nojit.count nojit.percent
 R_NilValue    LISTSXP R_NilValue     ENVSXP     4.88B       93.6%       5.45B        94.36%
 R_NilValue R_NilValue R_NilValue     ENVSXP    325.7M       6.23%     318.31M          5.5%
 R_NilValue R_NilValue     VECSXP     ENVSXP     6.32M       0.12%       5.55M         0.09%
    LISTSXP R_NilValue     VECSXP     ENVSXP     1.81M       0.03%        1.8M         0.03%
 R_NilValue R_NilValue     VECSXP R_NilValue    20.28K         <1%      20.27K           <1%
    LISTSXP    LISTSXP R_NilValue     ENVSXP    15.02K         <1%      15.02K           <1%
 R_NilValue    LISTSXP R_NilValue R_NilValue    11.09K         <1%      11.15K           <1%
 R_NilValue R_NilValue R_NilValue R_NilValue     3.23K         <1%       3.23K           <1%
    LISTSXP R_NilValue  EXTPTRSXP     ENVSXP         2         <1%           2           <1%
\end{example}

There we find some weirdness. Environments can either be hashtab-based or list-based.
Hashtab-based environments have a LISTSXP in the \code{frame} field, whereas, the
hashtab-based environments have a vector in the \code{hashtab} field. I assumed
hashtab-based was default, but what we actually see is this:

\begin{example}
 implementation jit.count jit.percent nojit.count nojit.percent
          empty   325.71M       6.23%     318.31M          5.5%
        hashtab     8.16M       0.15%       7.38M         0.12%
           list     4.88B       93.6%       5.45B        94.36%
           <NA>         2         <1%           2           <1%
\end{example}

There are ~93.5\% list-based environments, and less than 1\% hashtab-based
environments. There are also around 6\% environments that do not have an
internal representation. Finally, there is one case I don't understand:

\begin{example}
     attrib      frame    hashtab     enclos jit.count jit.percent nojit.count nojit.percent
    LISTSXP R_NilValue  EXTPTRSXP     ENVSXP         2         <1%           2           <1%
\end{example}

I do not understand why there is an external pointer in the \code{hashtab} field.
This happens only in two vignettes, one for package \code{HistogramTools} and one
for \code{RProtoBuf}. I looked at the former and found one example. Specifically,
it's hashtab is defined as follows. I think that attribute is very telling.


\begin{example}
@555563f18da0 22 EXTPTRSXP g0c0 [OBJ,NAM(3),ATT] 
ATTRIB:
  @555563f18d68 02 LISTSXP g0c0 [] 
    TAG: @555555c0cbd0 01 SYMSXP g1c0 [MARK,NAM(3),LCK,gp=0x4000] "class" (has value)
    @55555d7363c8 16 STRSXP g0c1 [NAM(3)] (len=1, tl=0)
      @55555d4dcbb8 09 CHARSXP g0c3 [gp=0x61,ATT] [ASCII] [cached] "UserDefinedDatabase"
\end{example}

As per usual, I do a stack trace at the moment of allocation.

\begin{example}
#1  0x00005555556d998b in Rf_allocSExp (t=4) at memory.c:2316
#2  0x000055555567405b in do_attach (call=0x555556140e50, op=0x555555c1bac8, args=0x555563f20240, env=0x555563f20320) at envir.c:2430
#3  0x0000555555693625 in bcEval (body=0x555556124308, rho=0x555563f20320, useCache=TRUE) at eval.c:6781
#4  0x0000555555681097 in Rf_eval (e=0x555556124308, rho=0x555563f20320) at eval.c:624
#5  0x0000555555683752 in R_execClosure (call=0x555563f18c18, newrho=0x555563f20320, sysparent=0x555555c581c8, rho=0x555555c581c8, arglist=0x555563f20518, op=0x555556124688)
    at eval.c:1773
#6  0x0000555555683470 in Rf_applyClosure (call=0x555563f18c18, op=0x555556124688, arglist=0x555563f20518, rho=0x555555c581c8, suppliedvars=0x555555c0cee0) at eval.c:1701
\end{example}

We find that the initialization happens within a function called
\code{do\_attach} (which is really long). This is what it does:

\begin{example}
  To attach a list we make up an environment and insert components
  of the list in as the values of this env and install the tags from
  the list as the names.
\end{example}

We specifically find ourselves in the following fragment of that function:

\begin{example}
else { /* is a user object */
	/* Having this here (rather than below) means that the onAttach routine
	   is called before the table is attached. This may not be necessary or
	   desirable. */
	R_ObjectTable *tb = (R_ObjectTable*) R_ExternalPtrAddr(CAR(args));
	if(tb->onAttach)
	    tb->onAttach(tb);
	PROTECT(s = allocSExp(ENVSXP));                                   // <----- WE ARE HERE
	SET_HASHTAB(s, CAR(args));                                        // CAR(args) is an EXTPTRSXP
                                                                        // we set it to be HASHTAB
	setAttrib(s, R_ClassSymbol, getAttrib(HASHTAB(s), R_ClassSymbol));
}
\end{example}

That's pretty straightforward then. We allow the environment to be implemented
as a user object, which means instead of a hashtable, we give it an external
pointer to the user object.
