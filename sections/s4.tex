\subsection{S4 objects}

The type \code{S4SXP} is used to express classes and objects in R's S4
object-oriented model. \footnote{
    R has four different object-oriented systems built in (that I know of): S3,
    S4, Reference Classes, and R6. Although there are also other custom built
   object-oriented systems in R. For instance \code{ggproto} is an object-oriented
   system built for and used by the \code{ggplot2} graphical library. From the
   documentation of \code{ggplot2}:

    *\code{ggproto} implements a prototype based OO system which blurs the lines
    between classes and instances. It is inspired by the proto package, but it
    has some important differences. Notably, it cleanly supports cross-package
    inheritance, and has faster performance.*

    *In most cases, creating a new OO system to be used by a single package is
    not a good idea. However, it was the least-bad solution for \code{ggplot}2
    because it required the fewest changes to an already complex code base.*

    I found the naming convention intriguing so I looked up its history. Here's
    a brief for your enjoyment. S3 and S4 were implemented in R's predecessor
    language, the S language. The S3 system was introduced in version 3 of S,
    and the S4 system was introduced in version 4 of S. Hence the names. When
    Reference Classes were being introduced in R ver. 2.12 to fix some of the
    problems with S4 they were jokingly referred to as R5, but the official name
    stuck (sometimes shortened to refclasses). Reference Classes are based
    strongly on S4. There was also an experimental unreleased object oriented
    system started by Simon Urbanek meant to solve some performance and
    usability issues with S4. R6 is named as a successor to those two, since it
    is very similar to refclasses, but more light-weight and without some
    issues of S4.

    More information is available in Object-Oriented Programming, Functional
    Programming and R
    (https://projecteuclid.org/download/pdfview\_1/euclid.ss/1408368569) 
    by John Chambers.
} S4 allows us to create classes, supports
inheritance and multiple dispatch. It also tends to be a little convoluted for
unaccustomed users. And for me.

Actually, while \code{S4SXP}s express S4 objects, other types of SEXPs can also
express S4 objects. What matters whether something is considered an S4 object
is not the type, but whether the S4 mask is set in the \code{gp} field of the SEXP's
header. The following snippet shows the definition of the mask and the basic
operations that set and reset an object's S4 status.

\begin{example}
#define S4_OBJECT_MASK     ((unsigned short)(1<<4))
#define IS_S4_OBJECT(x)    ((x)->sxpinfo.gp & S4_OBJECT_MASK)
#define SET_S4_OBJECT(x)   (((x)->sxpinfo.gp) |= S4_OBJECT_MASK)
#define UNSET_S4_OBJECT(x) (((x)->sxpinfo.gp) &= ~S4_OBJECT_MASK)
\end{example}

There is no dedicated payload type that would represent S4 objects. There
aren't even helpful macros like we've used for \code{BCODESXP}s. Instead, we are
stuck using \code{listsxp\_struct} to represent \code{S4SXP}s.  Fortunately, there is not
much to look at: \ref{fig:s4sxp}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{car}	    {INTSXP}{carval}{}      {};
    \nextSlot{cdr}	    {VECSXP}{cdrval}{}	    {};
    \nextSlot{tag}	    {NILSXP}{tagval}{}	    {52B};

    \sexpRangeBottom{value}{env}{listsxp\_struct};

    %\sexpLabelRight{lab}{17,5}{1.5cm}{code}{BCODE\_CODE(~)};
    %\sexpLabelRight{lab}{17,4}{1.5cm}{consts}{BCODE\_CONSTS(~)};
    %\sexpLabelRight{lab}{17,3}{1.5cm}{expr}{BCODE\_EXPR(~)};

\end{tikzpicture}
\caption{\label{fig:s4sxp} S4SXP.}
\end{figure}

What we see inside is two dangling pointers in \code{carval} and \code{cdrval} and a
pointer to \code{R\_NilValue} in \code{tagval}. This perplexing structure escapes any
insight I have developed so far, but I am *pretty sure* neither slot is used
for anything, since the \code{duplicate} function for \code{S4SXP}s seems to just
recreate this state exactly without copying anything. Instead, all the
information related to operating S4 objects is stored as attributes. I imagine
this is what allows S4 objects to be of any type, not only \code{S4SXP}.

So what attributes would we see in an S4 object? Let's investigate an example.
In order to do that we should first create a class:

\begin{example}
setClass("X", slots=c(x = "numeric", y = "numeric"))
\end{example}

This creates a class called \code{X} in the current environment with two slots
(fields), \code{x} and \code{y}, both of the numeric. What the interpreter actually does
is that it creates an object called \code{.\_\_C\_\_X} in the current environment that
represents our class and can be used as a factory for instantiating objects.
Let's fish it out from the environment and look at it:

\begin{example}
> x_class <- globalenv()$.__C__X
> x_class
Class "X" [in ".GlobalEnv"]

Slots:
                      
Name:        x       y
Class: numeric numeric
\end{example}

We see that \code{x\_class} is an \code{S4SXP} type object. The \code{inspect} function also
indicates that the S4 bit is set in the \code{gp} part of the header by printing
\code{S4} in the square brackets. The payload of \code{S4SXP} is not shown. We could peer
into it regardless, but it contains only those two dangling pointers and a
pointer to \code{R\_NilValue} so it wouldn't be productive.

Instead we can take a look at the mess of attributes. There is a lot of stuff
in the print out, but we summarize the attributes as follows. (We could also
list them using \code{attributes} in R). Tab.~\ref{tab:attr1}

\begin{table}
\centering
\begin{tabular}{ll}
\toprule
    \code{attr} & value \\ \midrule
    \code{slots} & \code{list(x="numeric",~y="numeric")}\\
    \code{contains} & \code{list()}\\
    \code{virtual} & \code{FALSE}\\
    \code{prototype} & an S4 object where \code{x=0, y=0}\\
    \code{validity} & \code{"\textbackslash{}001NULL\textbackslash{}001"}\\
    \code{access} & \code{list()}\\
    \code{className} & \code{"X"}\\
    \code{package} & \code{R\_GlobalEnv}\\
    \code{subclasses} & \code{list()}\\
    \code{versionKey} & an EXPRPTRSXP object\\
    \code{sealed} & \code{FALSE}\\
    \code{class} & \code{"classRepresentation"}\\
\bottomrule
\end{tabular}
\caption{\label{tab:attr1} }
\end{table}

Most of these fields are self-explanatory and it is probably beyond the scope of
this report to go into the details of all of them, but let's boringly discuss a few key
attributes.

The \code{slots} attribute contains the definitions of slots (fields) of class \code{X}.
The \code{contains} attribute lists the classes from which \code{X} inherits. The
\code{prototype} attribute gives us an object that will be used as a basis to create
instances of this class. The \code{validity} attribute optionally points to a
function for checking the validity of an object of class \code{X}. The \code{className}
attribute tells us the name of class \code{X}, which is contrasted with the \code{class}
attribute, which tells us what is the role of the object we're looking at: it's
a class definition. The \code{sealed} attribute says whether the class can be
modified or not. The \code{package} attribute specifies which environment this
object belongs to. 

Now let's define a method for the class. First we define a generic method which
we will call \code{concatenate} and which will take one argument called \code{object}:


\begin{example}
setGeneric("concatenate", function(object) standardGeneric("concatenate"))
\end{example}

This creates a function called \code{concatenate} in the current environment:

\begin{example}
> globalenv()$concatenate
standardGeneric for "concatenate" defined from package ".GlobalEnv"

function (object) 
standardGeneric("concatenate")
<environment: 0x55555d2328f0>
Methods may be defined for arguments: object
Use  showMethods("concatenate")  for currently available ones.
\end{example}

Further inspection would reveal that this is an ordinary \code{CLOSXP} closure which
invokes the dispatch mechanism for S4. Another thing that happens is that an
object called \code{.\_\_T\_\_concatenate:.GlobalEnv} is created in the environment.

For now the object is an empty hash table environment. Let's see how it changes
as we proceed to define the actual method. 


\begin{example}
setMethod("concatenate", signature(object = "X"), function(object) paste(object@x, object@y))
\end{example}

We define the signature as an object of class \code{X}---this will be used to guide
the dispatch. The body will concatenate the two fields of \code{object}, \code{x} and
\code{y}, using the standard \code{paste} function. Nothing surprising. We then test out
the method by calling it on our object \code{x} of class \code{X} and see it indeed
works. Adding a method impacts that \code{.\_\_T\_\_concatenate:.GlobalEnv} object in the
environment.

What happens is that the closure we created by defining a function is stored it
in the hash table under \code{X}, the class name used for multiple dispatch. In
addition, there are a number of attributes attached to the closure. There's a
lot there, so let's summarize them: Tab.~\ref{tab:attr2}

\begin{table}
\centering
\begin{tabular}{ll}
\toprule
    \code{attr} & value \\ \midrule
    \code{target} & S4 object of class \code{"signature"}\\
    \code{defined} & S4 object of class \code{"signature"}\\
    \code{generic} & \code{"concatenate"}\\
    \code{class} & \code{"methodDefinition"}\\
    \code{srcref} & source reference \code{c(1,51,1,92,51,92,1,1)}\\
\bottomrule
\end{tabular}
\caption{\label{tab:attr2} }
\end{table}

These attributes can be used to direct multiple dispatch using the types of all
objects passed to a generic function, since we have the signatures of methods
preserved right there.

Let's now create an object of class \code{X} and take a look at it.

\begin{example}
> x <- new("X", x=42, y=63)
> x
An object of class "X"
Slot "x":
[1] 42

Slot "y":
[1] 63
\end{example}

So far so good. Now let's look at it harder:

We see again an \code{S4SXP} type object and some arguments that are much fewer in
number than in the class definition. We could summarize them as follows:

\begin{table}
\centering
\begin{tabular}{ll}
\toprule
    \code{attr} & value \\ \midrule
    \code{x} & \code{42}\\
    \code{y} & \code{63}\\
    \code{class} & \code{"X"}\\
\bottomrule
\end{tabular}
\caption{\label{tab:attr3} }
\end{table}

These are straightforward and represent the state of the object. We also see
that the \code{class} attribute has its own attribute that points to the package of
the class.

