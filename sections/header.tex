\section{The rest of the header}

We discussed some parts of the SEXP header already, but there are some useful
flags in there that we have thus far omitted. Here is a rundown of what they
mean and how to use them. 

\subsection{Debug}

The \code{debug} flag is set only for SEXPs of type CLOSXP and ENVSXP. For CLOSXPs
it indicates whether the function is executed in debug mode. The debug mode
uses the browser which allows to step over instructions and observe values of
objects (like \code{gdb}). The browser can be turned on on-demand by calling
\code{browser()}, but functions in debug mode trigger the browser automatically. To
set/unset the debug flag for a closure use functions \code{debug} and \code{undebug},
like so:

\begin{example}
> f <- function(x) x + 1
> debug(f)
> f(1)       
...                      # runs with browser
> undebug(f)
> f(1)       
...                      # runs without browser
\end{example}

In the case of environments the debug flag indicates whether the environment is
going to be browsed in single-step mode.  

The inspect function shows that the debug flag is set by printing \code{DBG} in the
flag section, like so:

\begin{example}
> debug(f)
> .Internal(inspect(f))
@1995b298 03 CLOSXP g0c0 [MARK,NAM(2),DBG] 
\end{example}

There are macros that allow to read and modify these flags:

\begin{example}
#define RDEBUG(x)       ((x)->sxpinfo.debug)
#define SET_RDEBUG(x,v) (((x)->sxpinfo.debug)=(v))
#define RSTEP(x)        ((x)->sxpinfo.spare)
\end{example}

\subsubsection{Spare}

The \code{spare} flag is set on closures to mark them for one-time debugging with
reference counting. The flag can be set using \code{debugonce} and it is
automatically unset once the function is executed.

\begin{example}
> f <- function(x) x + 1
> debugonce(f)
\end{example}

The inspect function shows the spare flag as \code{STP}:

\begin{example}
> .Internal(inspect(f))
@1995b298 03 CLOSXP g0c0 [MARK,NAM(2),STP] 
\end{example}

\subsubsection{Trace}

The \code{trace} flag is set to indicate that we want to keep track of objects being
copied. The tracing can be turned on via function \code{tracemem} like so:

%<!-- footnote: http://adv-r.had.co.nz/memory.html --> 

\begin{example}
> d <- c(1:10)
> tracemem(d)
[1] "<0x55d2e3be8fa0>"
\end{example}

The inspect function will then print as follows, with the trace flag indicated
as \code{TR}:

\begin{example}
> .Internal(inspect(d))
@1aef6b18 13 INTSXP g0c4 [NAM(2),TR] (len=10, tl=0) 1,2,3,4,5,...
\end{example}

With this flag turned on, the interpreter will inform the programmer whenever
copying occurs. For instance, when executing an assignment the tracer will
print something like this:

\begin{example}
> d[5] <- 42
tracemem[0x55d2e3be8fa0 -> 0x55d2e20c8680]:
\end{example}
