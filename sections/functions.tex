\section{Functions}

From the point of view of SEXPs, there are three types of functions found in R:
closures, built-ins, and specials. Closures are lazy functions defined in R (e.g.
\code{print}). Built-ins are strict functions defined in C or other languages (e.g.
\code{+}). Specials are functions defined in C whose arguments are not interpreted,
but passed in as ASTs (e.g. \code{if}). They are collectively grouped under the
synthetic type \code{FUNSXP}.

\subsubsection{Closures}

A closure is represented by a \code{CLOSXP}. The payload of a closure's SEXP is
specified by a \code{closxp\_struct}, which contains pointers to three other SEXPs:
the closure's formals (argument definitions), its body, and its enclosing
environment: \ref{fig:closxp}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{formals}	{LISTSXP}{formals}{}    {};
    \nextSlot{body}	    {LANGSXP}{body}{}	    {};
    \nextSlot{env}	    {VECSXP}{env}{}		    {52B};

    %\sexpRange{sxpinfo}{gengcn}{SEXPREC\_HEADER};
    \sexpRangeBottom{formals}{env}{closxp\_struct};

    \sexpLabelRight{lab}{17,5}{1.5cm}{formals}{FORMALS(~)};
    \sexpLabelRight{lab}{17,4}{1.5cm}{body}{BODY(~)};
    \sexpLabelRight{lab}{17,3}{1.5cm}{env}{CLOENV(~)};
\end{tikzpicture}
\caption{\label{fig:closxp} CLOSXP.}
\end{figure}

The formals are a list of formal arguments that the function accepts expressed
as a pairlist (\code{LISTSXP}). If the function has no formal arguments, \code{formals}
points to \code{R\_NilValue}. If there are formal arguments, there is a list where
the \code{tagval}s point to symbols representing the names of arguments, and
\code{carval}s point to their values, if the arguments have default values. In
arguments that do not have values, \code{carval} is set to \code{R\_UnboundValue}.  Let us
illustrate with the following example:

\begin{example}
function(x, y, z=1) x + y + z
\end{example}

Its formals are structured as follows: \ref{fig:formals}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    %\sexpHeader{(0,5)}{};
    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{formals}	{LISTSXP}{formals}{\color{green!30} \textbullet} {};
    \nextSlot{body}	    {LANGSXP}{body}{\color{green!30} x + y + z}	     {};
    \nextSlot{env}	    {VECSXP}{env}{\color{green!30} R\_GlobalEnv}     {52B};

    \sexpHeaderOmitted{(0,-6)}{header1};
    \nextSlot{car1}	{SYMSXP}{carval}{\color{red} R\_Unbound...}       {};
    \nextSlot{cdr1}	{LISTSXP}{cdrval}{\color{green!30} \textbullet}	    {};
    \nextSlot{tag1}	{SYMSXP}{tagval}{\color{green!30} x}            {52B};

    \sexpPointerDown{frame}{header1}{1.5cm};

    \sexpHeaderOmitted{(0,-12)}{header2};
    \nextSlot{car2}	{SYMSXP}{carval}{\color{red} R\_Unbound...}  {};
    \nextSlot{cdr2}	{LISTSXP}{cdrval}{\color{green!30} \textbullet}	    {};
    \nextSlot{tag2}	{SYMSXP}{tagval}{\color{green!30} y}            {52B};

    \sexpPointerDown{cdr1}{header2}{1.5cm};

    \sexpHeaderOmitted{(0,-18)}{header3};
    \nextSlot{car3}	{REALSXP}{carval}{\color{green!30} 1.0}	        {};
    \nextSlot{cdr3}	{NILSXP}{cdrval}{\color{red} R\_NilValue}	    {};
    \nextSlot{tag3}	{SYMSXP}{tagval}{\color{green!30} z}            {52B};

    \sexpPointerDown{cdr2}{header3}{1.5cm};

\end{tikzpicture}
\caption{\label{fig:formals} \code{function(x, y, z=1) x + y + z}.}
\end{figure}

The body is an AST representing the body of the function. The enclosing
environment is the environment in which the function operates. These follow the
structure we have laid out earlier.

All three elements can be accessed in R via their eponymous functions:

\begin{example}
> f <- function(x) x + 1
> formals(f)
$x
> body(f)
x + 1
> environment(f)
<environment: R_GlobalEnv>
\end{example}

They can also be accessed via macros in C:

\begin{example}
#define FORMALS(x)	((x)->u.closxp.formals)
#define BODY(x)		((x)->u.closxp.body)
#define CLOENV(x)	((x)->u.closxp.env)
\end{example}

\subsubsection{Built-ins and specials}

Built-ins are represented by SEXPs of type \code{BUILTINSXP} and specials by
\code{SPECIALSXP}. These both have a very simple structure described by the
\code{primsxp\_struct} structure accessible via the \code{primsxp} field of the payload
structure. Unlike the other payloads, \code{primsxp\_struct} contains only a single
integer:

\begin{example}
struct primsxp_struct {
    int offset;
};
\end{example}

The R interpreter has a table specifying internal and primitive functions. It
is defined in \code{names.c} as \code{R\_FunTab}. The \code{offset} field in \code{primsxp\_struct}
is an index for that table. This, given a \code{BUILTINSXP} or a \code{SPECIALSXP}, we
can get more information about the function it describes by reaching for its
offset and retrieving the element specified by that offset from \code{R\_FunTab}:

\begin{example}
SEXP s;
int index = s->u.primsxp.offset;
FUNTAB function_info = R_FunTab[index];
\end{example}

The information about a function is described by the \code{FUNTAB} structure:

\begin{example}
typedef struct {
    char   *name;    /* print name */
    CCODE  cfun;     /* c-code address */
    int	   code;     /* offset within c-code */
    int	   eval;     /* evaluate args? */
    int	   arity;    /* function arity */
    PPinfo gram;     /* pretty-print info */
} FUNTAB;
\end{example}

The \code{name} and \code{arity} of the function are self-explanatory. The other fields
require some interpretation.  The \code{cfun} field contains the address of the
function. The \code{CCODE} type is a pointer to a four argument function defined as
follows:

\begin{example}
typedef SEXP (*CCODE)(SEXP,  /* call expression     -- LANGSXP */
                      SEXP,  /* function expression -- CLOSXP  */
                      SEXP,  /* argument list       -- LISTSXP */
                      SEXP); /* environment (rho)   -- ENSXP   */
\end{example}

Further, the \code{code} variable specifies the variant of the function which should
be used. For instance, the operators \code{<-}, \code{<<-}, and \code{=} are all defined
within the single function \code{do\_set}, but their \code{code} values are 1, 2, and 3,
respectively. This is used within the \code{do\_set} function, as follows:


\begin{example}
SEXP attribute_hidden do_set(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    /* ... */

    rhs = eval(CADR(args), rho);
    INCREMENT_NAMED(rhs);
    if (PRIMVAL(op) == 2)                           /* <<- */
        setVar(lhs, rhs, ENCLOS(rho));              /* define value in enclosing environment */
    else                                            /* <-, = */
        defineVar(lhs, rhs, rho);                   /* define value in current environment */
    R_Visible = FALSE;
    return rhs;
    
   /* ... */
}
\end{example}

The \code{eval} field contains information about how the function and its arguments
are supposed to be evaluated. It's format is that of a  single three digit
integer where each digit contains one separate piece of information, summarized
as follows:

MISSING FIGURE

If the least significant digit is 1, then the arguments are evaluated before
calling the function (ie. built-in semantics). Alternatively, if the least
significant digit is 0, the arguments are passed without evaluating (ie.
special semantics). If the middle digit is 1, then the function must be
executed via the \code{.Internal} function in R. The most significant digit can
either be 0, 1, or 2. It specifies whether \code{R\_Visible} should be turned on or
off for this function. If \code{R\_Visible} is turned on, the function prints its
result if called from the interpreter. If it is turned off, the function's
result is not printed on the interpreter. If the most significant digit is set
to 1, the \code{R\_Visible} is forced on, and for 0 it is forced off. If the digit
is 2, then \code{R\_Visible} is turned on, but it can subsequently be modified by C
code.

Last field! \code{gram}! It is a structure that is used to guide the pretty printer:

\begin{example}
typedef struct {
    PPkind kind; 	     /* deparse kind */
    PPprec precedence;       /* operator precedence */
    unsigned int rightassoc; /* right associative? */
} PPinfo;
\end{example}

\code{PPkind} and \code{PPprec} are enums which specify different kinds of expressions
(e.g. function, binary call, if statement) and different precedence values (17
specific precedence levels). The \code{rightassoc} is 1 if the expression associates
to the right, and 0 if it does not. 

As an example, here are some rows from the primitives and internals table:

\begin{example}
{"if",    do_if,     0,      200, -1, {PP_IF,      PREC_FN,  1}},
{"+",     do_arith,  1,      1,   -1, {PP_BINARY,  PREC_SUM, 0}},
{"length",do_length, 0,      1,    1, {PP_FUNCALL, PREC_FN,  0}},
\end{example}

There are macros that provide an easy way to access both the value of the
offset for a given \code{BUILTINSXP} or \code{SPECIALSXP}, as well as to access the
fields of the \code{FUNTAB} structure, including some interpretation:

\begin{example}
#define PRIMOFFSET(x)                ((x)->u.primsxp.offset)
#define SET_PRIMOFFSET(x,v)         (((x)->u.primsxp.offset)=(v))
#define PRIMFUN(x)          (R_FunTab[(x)->u.primsxp.offset].cfun)
#define PRIMNAME(x)         (R_FunTab[(x)->u.primsxp.offset].name)
#define PRIMVAL(x)          (R_FunTab[(x)->u.primsxp.offset].code)
#define PRIMARITY(x)        (R_FunTab[(x)->u.primsxp.offset].arity)
#define PPINFO(x)           (R_FunTab[(x)->u.primsxp.offset].gram)
#define PRIMPRINT(x)      (((R_FunTab[(x)->u.primsxp.offset].eval)/100)%10)
#define PRIMINTERNAL(x)   (((R_FunTab[(x)->u.primsxp.offset].eval)%100)/10)
\end{example}
