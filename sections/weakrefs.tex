\subsection{Weak references}

Weak references are references that are not protected from garbage collection,
but are associated with a finalizer that is run when they become unreachable.

Internally, a weak reference is represented by its own type: \code{WEAKREFSXP}.
In actuality, it is a special sub-type of \code{VECSXP} which always has length 4.
The elements of the vector represent a \code{key}, a \code{value}, a \code{finalizer}, and
\code{next}. While there is no special struct to retrieve the information from
\code{WEAKREFSXPs}, there are macros which aid retrieving the elements of the
vector:

\begin{example}
#define WEAKREF_KEY(w)       VECTOR_ELT(w, 0)
#define WEAKREF_VALUE(w)     VECTOR_ELT(w, 1)
#define WEAKREF_FINALIZER(w) VECTOR_ELT(w, 2)
#define WEAKREF_NEXT(w)      VECTOR_ELT(w, 3)
\end{example}

Thus we can represent weak references as follows: \ref{fig:}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 4}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextSlot{e0}		    {SEXP}{[0]}{\color{green!30} ~}                   {};
    \nextSlot{e1}		    {SEXP}{[1]}{\color{green!30} ~}                        {};
    \nextSlot{e2}		    {FUNSXP}{[2]}{\color{green!30} ~} {};
    \nextSlot{e3}		    {WEAKREFSXP}{[3]}{\color{green!30} ~}                        {};

    \sexpLabelRightWide{lab}{31.5,6}{1.5cm}{e0}{WEAKREF\_NEXT(~)};
    \sexpLabelRightWide{lab}{31.5,5}{1.5cm}{e1}{WEAKREF\_FINALIZER(~)};
    \sexpLabelRightWide{lab}{31.5,4}{1.5cm}{e2}{WEAKREF\_VALUE(~)};
    \sexpLabelRightWide{lab}{31.5,3}{1.5cm}{e3}{WEAKREF\_KEY(~)};

\end{tikzpicture}
\caption{\label{fig:extptrsxp} EXTPTRSXP.}
\end{figure}

The key and value represent the relationships between weak references and data
that the weak reference carries. The \code{key} is either \code{R\_NilValue}, an
environment or an external pointer. A weak reference can be garbage collected
when it is unreachable. The value is reachable if it is either reachable
directly or via weak references with reachable keys. Once a value is determined
to be unreachable during garbage collection, the key and value are set to
\code{R\_NilValue}. Also bit \code{0} in the \code{gp} portion of the header is set to \code{1} to
indicate readiness to finalize. The finalizer is either a function or a pointer
to \code{R\_NilValue}.  This is the function that will be run when the weak reference
is actually garbage collected. The next field is used to connect all weak
references into a single structure. When created, a weak reference sets its
next field to \code{R\_weak\_refs} and then overwrites \code{R\_weak\_refs} with a pointer to
itself. Finally, the user can also set \code{gp} bit in position \code{1} to indicate
that the weak reference should be garbage collected on exit.

There are macros to facilitate setting those \code{gp} bits:

\begin{example}
#define READY_TO_FINALIZE_MASK 1

#define SET_READY_TO_FINALIZE(s)   ((s)->sxpinfo.gp |= READY_TO_FINALIZE_MASK)
#define CLEAR_READY_TO_FINALIZE(s) ((s)->sxpinfo.gp &= ~READY_TO_FINALIZE_MASK)
#define IS_READY_TO_FINALIZE(s)    ((s)->sxpinfo.gp & READY_TO_FINALIZE_MASK)

#define FINALIZE_ON_EXIT_MASK 2

#define SET_FINALIZE_ON_EXIT(s)    ((s)->sxpinfo.gp |= FINALIZE_ON_EXIT_MASK)
#define CLEAR_FINALIZE_ON_EXIT(s)  ((s)->sxpinfo.gp &= ~FINALIZE_ON_EXIT_MASK)
#define FINALIZE_ON_EXIT(s)        ((s)->sxpinfo.gp & FINALIZE_ON_EXIT_MASK)
\end{example}

Unfortunately, I have not seen a weak reference in the wild yet so I have no
examples.

