\section{Byte code}

R is an interpreted language, but the interpreter comes in two forms. There's
the AST interpreter which runs SEXPs as they are, and there's the byte code
interpreter, which runs byte code. Byte code is generated from ASTs using the
byte code compiler. The compiler is a just-in-time compiler that is turned on by
default in the newest versions of R. Otherwise you can turn it on by setting
the environment variable \code{R\_ENABLE\_JIT} to \code{3} before running R (and to \code{0}
to turn the JIT off and \code{1}--\code{2} for intermediate settings\footnote{JIT settings:
\begin{tabular}{lll}
\toprule
    \code{R\_ENABLE\_JIT} & meaning\\ \midrule
    \code{0} & JIT compilation is disabled\\
    \code{1} & larger closures are compiled before they are used for the first time\\
    \code{2} & like above, but some small closures are also compiled before their second use\\
    \code{3} & like above, but all top-level loops are compiled before they are executed\\
\bottomrule
\end{tabular}
}). If
the JIT is running whenever you execute a function twice, it gets byte code
compiled before the second execution:`

\begin{example}
> f <- function(x) x + 1
> f
function(x) x + 1
> f(1)
[1] 2
> f(1)
[1] 2
> f
function(x) x + 1
<bytecode: 0x55555ae04870>
\end{example}

We see that after the second execution the interpreter says the function
contains byte code. We can also byte code compile functions and arbitrary
expressions on demand using the \code{compiler} package, e.g.:

\begin{example}
> library(compiler)
> f <- cmpfun(function(x) x + 1)
> f
function(x) x + 1
<bytecode: 0x55555b4d0c00>
> expr <- compile(42)
> expr
<bytecode: 0x5555573ff110>    
\end{example}

Let us concentrate on expressions for now. Internally, a byte code compiled
expression is represented by a SEXP of type \code{BCODESXP}. There is no dedicated
payload type that would represent byte code expressions. Instead, we use
\code{listsxp\_struct} to represent them, which we will access via the \code{listsxp}
field of the payload union. However, there are macros for accessing \code{BCODESXP}s
that more clearly specify the contents of the structure:

\begin{example}
#define BCODE_CODE(x)   CAR(x)
#define BCODE_EXPR(x)   TAG(x)
#define BCODE_CONSTS(x) CDR(x)
\end{example}

Thus we represent a \code{BCODESXP} as follows: \ref{fig:bcodesxp}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{code}	    {INTSXP}{carval}{}      {};
    \nextSlot{consts}	{VECSXP}{cdrval}{}	    {};
    \nextSlot{expr}	    {NILSXP}{tagval}{}	    {52B};

    \sexpRangeBottom{value}{env}{listsxp\_struct};

    \sexpLabelRight{lab}{17,5}{1.5cm}{code}{BCODE\_CODE(~)};
    \sexpLabelRight{lab}{17,4}{1.5cm}{consts}{BCODE\_CONSTS(~)};
    \sexpLabelRight{lab}{17,3}{1.5cm}{expr}{BCODE\_EXPR(~)};

\end{tikzpicture}
\caption{\label{fig:bcodesxp} BCODESXP.}
\end{figure}

The \code{BCODE\_CODE} slot points to a vector of operation codes and their
arguments. The \code{BCODE\_CONSTS} slot points to a vector of constant expressions
describing various elements of the SEXP including typically constants referenced
to by the expression prior to compilation (the AST), the operations' arguments,
the index of current expressions, and the source reference. 

The \code{BCODE\_EXPR} slot looks like it was initially intended to store the AST of
the uncompiled expression, but this does not seem to be the case currently. The
byte code compiler (function \code{do\_mkcode} in \code{eval.c} simply omits this field
when compiling an expression and the macro is not used anywhere in the code to
access it.

\subsubsection{Simple example}

Let us first take a look at both \code{BCODE\_CODE} and \code{BCODE\_CONSTS} and explain
how it works using a very simple example:

\begin{example}
> library(compiler)
> expr <- compile(42)
> expr
<bytecode: 0x5555573ff110>
\end{example}

We have loaded the \code{compiler} package and compiled an expression containing
just the number \code{42} into some byte-code. If we were to decompile \code{expr} using
some pretty printer (alas I don't know of any good ones) we would get the
following:

\begin{example}
LDCONST 42
RETURN
\end{example}

Let's first take a look at \code{BCODE\_CODE} of this expression. We can represent
it like this: \ref{fig:simpleexample}


\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{code}	    {INTSXP}{carval}{\color{green!30} \textbullet}   {};
    \nextSlot{consts}	{VECSXP}{cdrval}{\color{green!30} \textbullet}   {};
    \nextSlot{expr}	    {NILSXP}{tagval}{\color{red} R\_NilValue}	     {52B};

    \sexpLabelRight{lab}{17,5}{1.5cm}{code}{BCODE\_CODE(~)};
    \sexpLabelRight{lab}{17,4}{1.5cm}{consts}{BCODE\_CONSTS(~)};
    \sexpLabelRight{lab}{17,3}{1.5cm}{expr}{BCODE\_EXPR(~)};

    \sexpHeaderOmitted{(0,-19)}{header1};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 8}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextHalf{e0}		    {int}{[0]}{\color{green!30} 10}                 {};
    \nextHalf{e1}		    {int}{[1]}{\color{red} 0}                       {};
    \nextHalf{e2}		    {int}{[2]}{\small\color{green!30} 1432968343}   {};
    \nextHalf{e3}		    {int}{[3]}{\color{green!30} 21845}              {};
    \nextHalf{e4}		    {int}{[4]}{\color{red} 0}                       {};
    \nextHalf{e5}		    {int}{[5]}{\color{red} 0}                       {};
    \nextHalf{e6}		    {int}{[6]}{\small\color{green!30} 1432961375}   {};
    \nextHalf{e7}		    {int}{[7]}{\color{green!30} 21845}              {76B};

    \sexpPointerDown{code}{header1}{1.5cm};

    \sexpHeaderOmitted{(7.5,-7)}{header2};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 2}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextSlot{c0}		    {REALSXP}{[0]}{\color{green!30} 42}             {};
    \nextSlot{c1}		    {INTSXP}{[1]}{\color{green!30} \textbullet}     {60B};

    \sexpPointerDown{consts}{header2}{1.5cm};

    \sexpHeaderOmitted{(7.5,-14)}{header3};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 4}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextHalf{i0}		    {int}{[0]}{\color{red} NA\\{\small-2147483648}} {};
    \nextHalf{i1}		    {int}{[1]}{\color{red} 0}                       {};
    \nextHalf{i2}		    {int}{[2]}{\color{red} 0}                       {};
    \nextHalf{i3}		    {int}{[3]}{\color{red} 0}                       {60B};

    \sexpPointerDown{c1}{header3}{1.5cm};
\end{tikzpicture}
\caption{\label{fig:simpleexample} Simple byte-code expression.}
\end{figure}

Let's try to make sense of this. It's a vector containing 8 elements which
somehow map to either operations or operations' arguments. This does not make
sense, since there are only 2 operations in our code, one of which has 1
argument. So we would expect there to be 3 elements not 8.

Well, the vector is a vector of type \code{INTSXP}, containing integers, but
operations and arguments are both expressed as the following union, where \code{v}
can point to an operation and \code{i} can be used to express an argument.

\begin{example}
typedef union { void *v; int i; } BCODE;
\end{example}

This union has a potentially different size than \code{int}, so every element in the
\code{INTSXP} vector can take up more than one element. The number of elements that
is used by a single byte code operation is defined as follows.

\begin{example}
int m = (sizeof(BCODE) + sizeof(int) - 1) / sizeof(int);
\end{example}

On my system this number is \code{2}, so each operation will take up two vector
elements. So, it's a lie that this vector should be read as an integer vector.
Instead, what we need to do to understand the byte code, is to cast the integer to a BCODE vector.
This is how we'd do it for the example vector \code{expr} using gdb:

\begin{example}
> .Internal(inspect(expr))
@0x5555573ff110 21 BCODESXP g0c0 [NAM(1)]
^C
(gdb) (BCODE *) INTEGER(BCODE_CODE(s));
$1 = (BCODE *) 0x55555f29b0c8
\end{example}

Let us represent and compare the two vectors side by side: \ref{fig:vectorcomp}


\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \halfSlot{(-1.2,0)}{e0} {int}{[0]}{\color{green!30} 10}                 {};
    \nextHalf{e1}		    {int}{[1]}{\color{red} 0}                       {};
    \nextHalf{e2}		    {int}{[2]}{\small\color{green!30} 1432968343}   {};
    \nextHalf{e3}		    {int}{[3]}{\color{green!30} 21845}              {};
    \nextHalf{e4}		    {int}{[4]}{\color{red} 0}                       {};
    \nextHalf{e5}		    {int}{[5]}{\color{red} 0}                       {};
    \nextHalf{e6}		    {int}{[6]}{\small\color{green!30} 1432961375}   {};
    \nextHalf{e7}		    {int}{[7]}{\color{green!30} 21845}              {76B};

    %\sexpHeaderOmitted{(0,-6)}{header2};
    %\nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 4}         {};
    %\nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    %\nextSlot{b0}		    {int}{[0]}{\large\color{green!30} \{i=10, v=0x10\} }                 {};
    \sexpSlot{(0,-6.5)}{b0} {BCODE}{[0]}{\large\color{green!30} \{i=10, v=0x10\} }                 {};
    \nextSlot{b1}		    {BCODE}{[1]}{\large\color{green!30} \{i=14329683430, v=0x555555695c97\}} {};
    \nextSlot{b2}		    {BCODE}{[2]}{\large\color{red} \{i=0, ~~~~ v=0x0\}}   {};
    \nextSlot{b3}		    {BCODE}{[3]}{\large\color{green!30} \{i=1432961375, v=0x55555569415f\}} {76B};

    \sexpLabelRight{lab}{2,2.5}{1.5cm}{e0}{INTEGER(~)};
    \sexpLabelRight{lab}{2,-4}{1.5cm}{b0}{(BCODE *) INTEGER(~)};

\end{tikzpicture}
\caption{\label{fig:vectorcomp} Byte code vector representation comparison.}
\end{figure}

This makes more sense, but we still have 4 elements. But the first element in
the \code{BCODE} vector is actually neither an operation nor an argument. Instead it
is the version of the byte code compiler that was used to compile this SEXP. In
our case, this is version \code{10}. The other three elements are the operations and
their arguments, as expected.

How do we decode these? There is an array of operations called \code{opinfo} which
contains the address of the operation, the number of arguments it takes, and
its printable name:

\begin{example}
volatile
static struct { void *addr; int argc; char *instname; } opinfo[OPCOUNT];
\end{example}

The \code{addr} field of the array corresponds to the \code{v} fields of \code{BCODE}
operations. So for each operation we can search through \code{opinfo} to find such
\code{addr} that equals \code{v} and thus figure out what operation it is. 

There is a function in \code{eval.c} that does it called \code{R\_bcDecode}. It traverses the BCODE
vector looking up operations with the \code{findOp} function. The \code{findOp} function does an extremely straightforward search through \code{opinfo} comparing the addresses of the operations defined there versus the address of the operation we are looking for until there is a match:

\begin{example}
static int findOp(void *addr)
{
    int i;

    for (i = 0; i < OPCOUNT; i++)
	if (opinfo[i].addr == addr)
	    return i;
    error(_("cannot find index for threaded code address"));
    return 0; /* not reached */
}
\end{example}

We can also read the number of arguments from \code{argc} which tells us how to
treat subsequent elements in the vector. 

Let's get back to our example. In our case a search through
\code{opinfo} would yield the following operations for element 1's address:

\begin{example}
(gdb) p ((BCODE *) 0x55555f29b0c8)[1]
$2 = {v=0x555555695c97, i = 1432968343}
(gdb) p findOp($2.v)
$3 = {addr = 0x555555695c97 <bcEval+7996>, argc = 1, instname = 0x55555580bd12 "LDCONST"}
\end{example}

We see that this is the \code{LDCONST} (load constant) operation. It also tells us
that \code{LDCONST} takes one argument. From this we can figure out that element 2 of our vector is an argument for that operation, and element 3 is another operation. If we look up element 3 in \code{opinfo} we get the following information:

\begin{example}
(gdb) p findOp(((BCODE *) 0x55555f29b0c8)[3].v)
$4 = {addr = 0x55555569415f <bcEval+1028>, argc = 0, instname = 0x55555580bc60 "RETURN"}
\end{example}

We see that the operation is a \code{RETURN} operation that takes no arguments.

Let us go back to the argument to \code{LDCONST} though. The argument is not a value,
but instead it is an index to \code{BCODE\_CONSTS} that will yield the actual value
of the argument. Here, the argument's index is \code{0}. So let's take a look at
\code{BCODE\_CONSTS}.

In this example that vector is fairly simple: it only contains two entries. The
first entry (index \code{0}) of this vector is always the entire expression that
underwent compilation. So, if we wanted to 'decompile' a SEXP, we just need to
reach into \code{BCODE\_CONSTS} and take out the first element to do it. In our case
this is a \code{REALSXP} vector containing the single element \code{42}. Apart from
representing the entire compiled expression in our simple example this element
is also pointed to by the argument of the \code{LDCONST} operation---the entire
expressions as well as the loaded constant are the same, and the byte code
compiler tries not to duplicate entries in \code{BCODE\_CONSTS}.

The second element in \code{BCODE\_CONSTS} is an *expressions index*. This is an
integer vector that contains one entry per \code{(BCODE *)} element in \code{BCODE\_CODE}.
In our example we had 4 elements in \code{BCODE\_CODE}, so there are 4 elements in
the expressions index. Each element of this vector can be used to index
\code{BCODE\_CONSTS} to map operations in \code{BCODE\_CODE} to uncompiled expressions from
which they originated. This is trivial in the current example since we only
really have one expression in \code{BCODE\_CONSTS} but we will show a more
complicated example of this further below. The first element is (always) the
integer value of \code{NA}, since the compiler version should not be mapped to a
particular expression.

In summary, the entire expression can be represented as follows: \ref{fig:simpleexamplefull}

\begin{figure}
\centering
\begin{tikzpicture}[scale=.35, transform shape]

    \sexpHeaderOmitted{(0,0)}{header};
    \nextSlot{code}	    {INTSXP}{carval}{\color{green!30} \textbullet}   {};
    \nextSlot{consts}	{VECSXP}{cdrval}{\color{green!30} \textbullet}   {};
    \nextSlot{expr}	    {NILSXP}{tagval}{\color{red} R\_NilValue}	     {52B};

    \sexpLabelRight{lab}{17,5}{1.5cm}{code}{BCODE\_CODE(~)};
    \sexpLabelRight{lab}{17,4}{1.5cm}{consts}{BCODE\_CONSTS(~)};
    \sexpLabelRight{lab}{17,3}{1.5cm}{expr}{BCODE\_EXPR(~)};

    \sexpHeaderOmitted{(0,-19)}{header1};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 8}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextHalf{e0}		    {int}{[0]}{\color{green!30} 10}                 {};
    \nextHalf{e1}		    {int}{[1]}{\color{red} 0}                       {};
    \nextHalf{e2}		    {int}{[2]}{\small\color{green!30} 1432968343}   {};
    \nextHalf{e3}		    {int}{[3]}{\color{green!30} 21845}              {};
    \nextHalf{e4}		    {int}{[4]}{\color{red} 0}                       {};
    \nextHalf{e5}		    {int}{[5]}{\color{red} 0}                       {};
    \nextHalf{e6}		    {int}{[6]}{\small\color{green!30} 1432961375}   {};
    \nextHalf{e7}		    {int}{[7]}{\color{green!30} 21845}              {76B};

    \sexpSlot{(13.8,-23)}{b0}   {BCODE}{[0]}{\large\color{green!30} \{i=10, v=0x10\} }                 {};
    \nextSlot{b1}		    {BCODE}{[1]}{\large\color{green!30} \{i=14329683430, v=0x555555695c97\}} {};
    \nextSlot{b2}		    {BCODE}{[2]}{\large\color{red} \{i=0, ~~~~ v=0x0\}}   {};
    \nextSlot{b3}		    {BCODE}{[3]}{\large\color{green!30} \{i=1432961375, v=0x55555569415f\}} {76B};

    \sexpPointerDown{code}{header1}{1.5cm};

    \sexpHeaderOmitted{(7.5,-7)}{header2};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 2}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextSlot{c0}		    {REALSXP}{[0]}{\color{green!30} 42}             {};
    \nextSlot{c1}		    {INTSXP}{[1]}{\color{green!30} \textbullet}     {60B};

    \sexpPointerDown{consts}{header2}{1.5cm};

    \sexpHeaderOmitted{(7.5,-14)}{header3};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 4}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextHalf{i0}		    {int}{[0]}{\color{red} NA\\{\small-2147483648}} {};
    \nextHalf{i1}		    {int}{[1]}{\color{red} 0}                       {};
    \nextHalf{i2}		    {int}{[2]}{\color{red} 0}                       {};
    \nextHalf{i3}		    {int}{[3]}{\color{red} 0}                       {60B};

    \sexpPointerDown{c1}{header3}{1.5cm};

    \sexpPointerDownAndUp{b1}{c0}{2.5cm}{15cm}{24cm};
    \sexpPointerDownAndUp{b3}{c1}{1.5cm}{4cm}{22cm};

\end{tikzpicture}
    \caption{\label{fig:simpleexamplefull} Simple byte-code expression (full).}
\end{figure}

\subsubsection{A byte-code compiled function example}

Now that we have the details of \code{BCODESXP} down and a full toolkit, let's try
looking at a function.

\begin{example}
cmpfun( function(x, y) x + y )
\end{example}

This function is still represented by a \code{CLOSXP} like all other user-defined R
functions. The only difference is that the body of the function is not a
\code{LANGSXP}, but instead it is a \code{BCODESXP}. The whole thing can be represented
as follows: \ref{fig:functionexample}


\begin{figure}
\centering
\begin{tikzpicture}[scale=.25, transform shape]

    \sexpHeaderOmitted{(0,0)}{header0};
    \nextSlot{formals}	{LISTSXP}{formals}{\color{green!30} \textbullet} {};
    \nextSlot{body}	    {BCODESXP}{body}{\color{green!30} \textbullet}   {};
    \nextSlot{env}	    {ENVSXP}{env}{\color{green!30} R\_GlobalEnv}	 {52B};

    \sexpHeaderOmitted{(0,-6)}{header1};
    \nextSlot{car1}	    {SYMSXP}{carval}{\color{red} R\_Unbound..}      {};
    \nextSlot{cdr1}	    {LISTSXP}{cdrval}{\color{green!30} \textbullet} {};
    \nextSlot{tag1}	    {SYMSXP}{tagval}{\color{green!30} x}	        {52B};

    \sexpPointerDown{formals}{header1}{1.5cm};

    \sexpHeaderOmitted{(0,-12)}{header2};
    \nextSlot{car2}	    {SYMSXP}{carval}{\color{red} R\_Unbound..}      {};
    \nextSlot{cdr2}	    {NILSXP}{cdrval}{\color{red} R\_NilValue}       {};
    \nextSlot{tag2}	    {SYMSXP}{tagval}{\color{green!30} y}	        {52B};

    \sexpPointerDown{cdr1}{header2}{1.5cm};

    \sexpHeaderOmitted{(22,-12)}{header3};
    \nextSlot{code}	    {INTSXP}{carval}{\color{green!30} \textbullet}  {};
    \nextSlot{consts}	{VECSXP}{cdrval}{\color{green!30} \textbullet}  {};
    \nextSlot{expr}     {NILSXP}{tagval}{\color{red} R\_NilValue}	    {52B};
 
    \sexpLabelRight{lab}{39,-7}{1.5cm}{code}{BCODE\_CODE(~)};
    \sexpLabelRight{lab}{39,-8}{1.5cm}{consts}{BCODE\_CONSTS(~)};
    \sexpLabelRight{lab}{39,-9}{1.5cm}{expr}{BCODE\_EXPR(~)};

    \sexpPointerDown{body}{header3}{1.5cm};

    \sexpHeaderOmitted{(1,-25)}{header4};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 6}         {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		    {};
    \nextSlot{e0}		    {LANGSXP}{[0]}{\color{green!30} x + y\\~}                   {};
    \nextSlot{e1}		    {SYMSXP}{[1]}{\color{green!30} x\\~}                        {};
    \nextSlot{e2}		    {INTSXP}{[2]}{\color{green!30} 1,13,1,32\\13,32,1,1} {};
    \nextSlot{e3}		    {SYMSXP}{[3]}{\color{green!30} y\\~}                        {};
    \nextSlot{e4}		    {INTSXP}{[4]}{\color{green!30} \textbullet\\~}              {};
    \nextSlot{e5}		    {INTSXP}{[5]}{\color{green!30} NA,2,2,2\\2,2,2,2}  {92B};

    \sexpPointerDown{consts}{header4}{2.5cm};

    \sexpHeaderOmitted{(1,-31)}{header5};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 8}                  {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		             {};
    \nextHalf{i0}		    {int}{[0]}{\color{green!30} NA\\{\small-2147483648}}     {};
    \nextHalf{i1}		    {int}{[1]}{\color{green!30} 1\\~}                        {};
    \nextHalf{i2}		    {int}{[2]}{\color{green!30} 1\\~} {};
    \nextHalf{i3}		    {int}{[3]}{\color{green!30} 3\\~}                        {};
    \nextHalf{i4}		    {int}{[4]}{\color{green!30} 3\\~}              {};
    \nextHalf{i5}		    {int}{[5]}{\color{green!30} 0\\~}  {};
    \nextHalf{i6}		    {int}{[6]}{\color{green!30} 0\\~}  {};
    \nextHalf{i7}		    {int}{[7]}{\color{green!30} 0\\~}  {76B};

    \sexpPointerDown{e4}{header5}{1.3cm};

    \sexpHeaderOmitted{(0,-39.5)}{header6};
    \nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 13\\(for ints)}               {};
    \nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{red} 0}		                       {};
    \nextSlot{b0}		    {BCODE}{[0]}{\large\color{green!30} \{i=10, ~~~~ v=0x10\}}               {};
    \nextSlot{b1}		    {BCODE}{[1]}{\large\color{green!30} \{i=1432969285, v=0x555555696045\}}  {};
    \nextSlot{b2}		    {BCODE}{[2]}{\large\color{green!30} \{i=1,  ~~~~ v=0x1\}}                {};
    \nextSlot{b3}		    {BCODE}{[3]}{\large\color{green!30} \{i=1432969285, v=0x555555696045\}}  {};
    \nextSlot{b4}		    {BCODE}{[4]}{\large\color{green!30} \{i=3,  ~~~~ v=0x3\}}                {};
    \nextSlot{b5}		    {BCODE}{[5]}{\large\color{green!30} \{i=1432987413, v=0x55555569a715\}}  {};
    \nextSlot{b6}		    {BCODE}{[6]}{\large\color{green!30} \{i=0,  ~~~~ v=0x0\}}                {};
    \nextSlot{b7}		    {BCODE}{[7]}{\large\color{green!30} \{i=1432961375, v=0x55555569415f\}}  {108B};

    \sexpPointerDown{code}{header6}{1.5cm};

    \sexpNoteRight{lab}{52,-34}{1.5cm}{b1}{\code{LDVAL}};
    \sexpNoteRight{lab}{52,-35}{1.5cm}{b3}{\code{LDVAL}};
    \sexpNoteRight{lab}{52,-36}{1.5cm}{b5}{\code{ADD}};
    \sexpNoteRight{lab}{52,-37}{1.5cm}{b7}{\code{RETURN}};

    \sexpPointerDownAndUp{b2}{e0}{3.5cm}{32cm}{27.5cm};
    \sexpPointerDownAndUp{b4}{e1}{2.5cm}{21cm}{25.5cm};
    \sexpPointerDownAndUp{b6}{e3}{1.5cm}{10cm}{22.5cm};

    \sexpNoteRight{lab}{45.7,-19.5}{1.5cm}{e2}{\code{ATTRIB: class="srcref"}};
    \sexpNoteRight{lab}{47,-21.5}{1.5cm}{e4}{\code{ATTRIB: class="expressionsIndex"}};
    \sexpNoteRight{lab}{45.7,-22.5}{1.5cm}{e5}{\code{ATTRIB: class="srcref"}};
\end{tikzpicture}
    \caption{\label{fig:functionexample} Byte-code compiled function.}
\end{figure}

The structure of the \code{BCODESXP} is the same as we've seen before, but there are
a few additional elements that show up in \code{BCODE\_CONSTS}. Position \code{0} still
holds the entire uncompiled expression, which here is a \code{LANGSXP} representing
\code{2 + 2}. However, we also have two more expressions in positions \code{1} and \code{3}
representing variables.  We see that \code{LDVAR} operations in the \code{BCODE\_CODE}
vector have arguments pointing these variables. We also see that the \code{ADD}
operation's argument points to element \code{0} in \code{BCODE\_CONSTS}. The
\code{BCODE\_CONSTS} vector also contains an \code{expressionsIndex} vector just as
before, but it is no longer trivial. Let us therefore follow the mapping
between \code{BCODE\_CODE} and \code{BCODE\_CONSTS} that \code{expressionsIndex} describes: Tab.~\ref{tab:mapping}

\begin{table}
\centering
\begin{tabular}{lll}
\toprule
\code{BCODE\_CODE} & \code{expressionsIndex} & \code{BCODE\_CONSTS} \\ \midrule
\code{0} $\mapsto$ \code{10}      & 0 $\mapsto$ \code{NA} & \code{NA} $\mapsto$ \code{NA}\\
\code{1} $\mapsto$ \code{LDVAR}   & 1 $\mapsto$ \code{1}  & \code{1}  $\mapsto$ \code{x}\\
\code{2} $\mapsto$ \code{1}       & 2 $\mapsto$ \code{1}  & \code{1}  $\mapsto$ \code{x}\\
\code{3} $\mapsto$ \code{LDVAR}   & 3 $\mapsto$ \code{3}  & \code{3}  $\mapsto$ \code{y}\\
\code{4} $\mapsto$ \code{3}       & 4 $\mapsto$ \code{3}  & \code{4}  $\mapsto$ \code{y}\\
\code{5} $\mapsto$ \code{ADD}     & 5 $\mapsto$ \code{0}  & \code{0}  $\mapsto$ \code{x + y}\\
\code{6} $\mapsto$ \code{0}       & 6 $\mapsto$ \code{0}  & \code{0}  $\mapsto$ \code{x + y}\\
\code{7} $\mapsto$ \code{RETURN}  & 7 $\mapsto$ \code{0}  & \code{0}  $\mapsto$ \code{x + y}\\
\bottomrule
\end{tabular}
\caption{\label{tab:mapping} Mapping between \code{BCODE\_CODE} and \code{BCODE\_CONSTS}}
\end{table}

The mapping shows us that \code{LDVAR} operations come about by compiling the
individual variables. The \code{ADD} operation comes from compiling \code{+}. 
Finally, \code{RETURN} is associated with the last expression we compiled
(because we will return the value we obtain from the last expression).

Finally, the \code{BCODE\_CONSTS} contains two more \code{INTSXP} vectors, both with the
class attribute \code{srcref}. In a \code{BCODE\_CONSTS} vector all \code{INTSXP} vectors of
class \code{srcref} are source references for byte code operations, *except* for the
very last one. The *last* \code{srcref} is a mapping from elements in the \code{BCODE\_CODE}
vector to source references defined in \code{BCODE\_CONSTS}. In our example we only
have one source reference, so that mapping is trivial though. Nevertheless, let
us follow the mapping in detail: Tab.~\ref{tab:details}

\begin{table}
\centering
\begin{tabular}{lll}
\toprule
\code{BCODE\_CODE} & \code{srcref} & \code{BCODE\_CONSTS} \\ \midrule
\code{0} $\mapsto$ \code{10}      & 0 $\mapsto$ \code{NA} & \code{NA} $\mapsto$ \code{NA}\\
\code{1} $\mapsto$ \code{LDVAR}   & 1 $\mapsto$ \code{2}  & \code{2}  $\mapsto$ \code{1,13,1,32,13,32,1,1}\\
\code{2} $\mapsto$ \code{1}       & 2 $\mapsto$ \code{2}  & \code{2}  $\mapsto$ \code{1,13,1,32,13,32,1,1}\\
\code{3} $\mapsto$ \code{LDVAR}   & 3 $\mapsto$ \code{2}  & \code{2}  $\mapsto$ \code{1,13,1,32,13,32,1,1}\\
\code{4} $\mapsto$ \code{3}       & 4 $\mapsto$ \code{2}  & \code{2}  $\mapsto$ \code{1,13,1,32,13,32,1,1}\\
\code{5} $\mapsto$ \code{ADD}     & 5 $\mapsto$ \code{2}  & \code{2}  $\mapsto$ \code{1,13,1,32,13,32,1,1}\\
\code{6} $\mapsto$ \code{0}       & 6 $\mapsto$ \code{2}  & \code{2}  $\mapsto$ \code{1,13,1,32,13,32,1,1}\\
\code{7} $\mapsto$ \code{RETURN}  & 7 $\mapsto$ \code{2}  & \code{2}  $\mapsto$ \code{1,13,1,32,13,32,1,1}\\
\bottomrule
\end{tabular}
\caption{\label{tab:details} Detailed mapping between \code{BCODE\_CODE} and \code{BCODE\_CONSTS}}
\end{table}


From this we see that of the operations in our code come from source code
located in the environment, starting at line 1, byte 13 and ending at line 1
byte 32.

We can also disassemble the function to retrieve most of that information more
compactly:

\begin{example}
> disassemble(f)
list(.Code, list(10L, GETVAR.OP, 1L, GETVAR.OP, 3L, ADD.OP, 0L, 
    RETURN.OP), list(x + y, x, structure(c(1L, 13L, 1L, 32L, 
13L, 32L, 1L, 1L), srcfile = <environment>, class = "srcref"), 
    y, structure(c(NA, 1L, 1L, 3L, 3L, 0L, 0L, 0L), class = "expressionsIndex"), 
    structure(c(NA, 2L, 2L, 2L, 2L, 2L, 2L, 2L), class = "srcrefsIndex")))
\end{example}

