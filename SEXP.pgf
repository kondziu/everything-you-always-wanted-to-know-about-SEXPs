﻿%\usepackage{ifthen}
\usetikzlibrary{calc}
\usetikzlibrary{arrows.meta}

\newdimen\structureSizeInBytes
\makeatletter
\newcommand{\printStructureSizeInBytes} {
	\strip@pt\structureSizeInBytes\relax B
	%\ifthenelse{\structureSizeInBytes=0}{}{B}#4
}
\makeatother

\newcommand{\sexpSlot}[6]{
	\node[slot, text width=3.0cm, minimum width=5cm, minimum height=3.0cm] (#2) at #1 {{#4}\\{#5}};
	\node[slotType, below = 1.20cm, text width=3.5cm, minimum width=3.75cm] (#2Type) at (#2) {#3};
    \node[slotSize, above = 1.5cm, text width=5cm] (#2SizeStart) at (#2) {\printStructureSizeInBytes};
    \node[slotSize, above = 1.5cm, text width=5cm, align=right] (#2SizeEnd) at (#2) {#6};

    \def\lastSexpSlotNode{#2}
    \def\lastSexpSlotStep{5cm}
    \structureSizeInBytes=8pt
}

\newcommand{\halfSlot}[6]{
	\node[slot, text width=1.875cm, minimum width=2.5cm, minimum height=3.0cm] (#2) at #1 {{#4}\\{#5}};
	\node[slotType, anchor = south, text width=1.65cm, minimum width=1.75cm] (#2Type) at ([yshift=-.2cm] #2.south) {#3};
    \node[slotSize, anchor = south west] (#2SizeStart) at (#2.north west) {\printStructureSizeInBytes};
	\node[slotSize, anchor = south east] (#2SizeEnd) at (#2.north east) {#6};

    \def\lastSexpSlotNode{#2}
    \def\lastSexpSlotStep{2.5cm}
    \structureSizeInBytes=4pt
}

\newcommand{\nextSlot}[6]{
	\node[slot, text width=3.0cm, minimum width=5cm, minimum height=3.0cm,
          node distance=(\lastSexpSlotStep+5cm)/2, right of=\lastSexpSlotNode] (#1) {{#3}\\{#4}};
	\node[slotType, anchor = south, text width=3.5cm, minimum width=3.75cm] (#1Type) at ([yshift=-.2cm] #1.south) {#2};
	\node[slotSize, anchor = south west] (#1SizeStart) at (#1.north west) {\printStructureSizeInBytes};
	\node[slotSize, anchor = south east] (#1SizeEnd) at (#1.north east) {#5};

    \def\lastSexpSlotNode{#1}
    \def\lastSexpSlotStep{5cm}
	\advance\structureSizeInBytes by 8pt
}

\newcommand{\nextHalf}[6]{
	\node[slot, text width=1.875cm, minimum width=2.5cm, minimum height=3.0cm,
          node distance=(\lastSexpSlotStep+2.5cm)/2, right of=\lastSexpSlotNode] (#1) {{#3}\\{#4}};
	\node[slotType, anchor = south, text width=1.65cm, minimum width=1.75cm] (#1Type) at ([yshift=-.2cm] #1.south) {#2};
	\node[slotSize, anchor = south west] (#1SizeStart) at (#1.north west) {\printStructureSizeInBytes};
	\node[slotSize, anchor = south east] (#1SizeEnd) at (#1.north east) {#5};

    \def\lastSexpSlotNode{#1}
    \def\lastSexpSlotStep{2.5cm}
	\advance\structureSizeInBytes by 4pt
}

\newcommand{\nextAlign}[3]{
	\node[slot, fill=black!65, text width=1.875cm, minimum width=2.5cm, minimum height=3.0cm,
	      node distance=(\lastSexpSlotStep+2.5cm)/2, right of=\lastSexpSlotNode] (#1) {align};
    \node[slotSize, anchor = south west] (#1SizeStart) at (#1.north west) {\printStructureSizeInBytes};
    \node[slotSize, anchor = south east] (#1SizeEnd) at (#1.north east) {#2};

    \def\lastSexpSlotNode{#1}
    \def\lastSexpSlotStep{2.5cm}
}

\newcommand{\sexpHeader}[2] {
	\halfSlot{#1}		{sxpinfo}	{sxpinfo\_ struct}{sxp info}{}		{};
	\nextSlot        	{attrib}	{SEXP}{attrib}{}					{};
	\nextSlot			{gengcp}	{SEXP}{gengc\_ prev\_node}{}		{};
	\nextSlot			{gengcn}	{SEXP}{gengc\_ next\_node}{}        {#2};
}

\newcommand{\sexpHeaderOmitted}[2] {
	\node[text width=1.875cm, minimum width=2.5cm, minimum height=3.0cm] (#2) at #1 {};
	\draw[draw=white, fill=black!65] (#2.north west) -- 
									 (#2.north east) -- 
									 (#2.south east) -- 
									 (#2.south west) .. controls ++(1,1) and ++(-1,-1) .. 
									 (#2.north west);
	\node[omitted, text width=3.0cm] at (#2) [rotate=90]{SEXPREC\_ HEADER};
	%\node[slotSize, above = 1.5cm, text width=2.25cm, align=right] at (F) {28B};
	
	\def\lastSexpSlotNode{#2}
	\def\lastSexpSlotStep{2.5cm}
	\structureSizeInBytes=28pt
}

\newcommand{\nextDataOmitted}[2] {
	\node[text width=1.875cm, minimum width=2.5cm, minimum height=3.0cm,
		  node distance=(\lastSexpSlotStep+2.5cm)/2, right of=\lastSexpSlotNode] (#1) {};
	\draw[draw=white, fill=black!65] (#1.north west) -- 
									 (#1.north east) .. controls ++(-1,-1) and ++(1,1) ..
									 (#1.south east) -- 
									 (#1.south west) -- 
									 (#1.north west);
	\node[omitted, text width=3.0cm,] at (#1) [rotate=90]{DATAPTR};
	\node[slotSize, above = 1.5cm, text width=2.5cm] at (#1) {#2};

	\def\lastSexpSlotNode{#1}
    \def\lastSexpSlotStep{2.5cm}
}

\newcommand{\groupSlots}[4]{
	\draw[draw=black] ([yshift=.75cm] #2SizeStart.north west) -- 
								([yshift=.75cm, xshift=.5cm] #3SizeEnd.north east) --
								([yshift=-4cm, xshift=.5cm] #3SizeEnd.south east) --
								([yshift=-4cm] #2SizeStart.south west) --
								([yshift=.75cm] #2SizeStart.north west);
	\node[anchor=north west, font=\Large\tt] (#1) at ([yshift=.75cm] #2SizeStart.north west) {#4};

}

\newcommand{\sexpRange}[3]{
	\draw[{Triangle}-{Triangle}] 	([xshift=.5] #1SizeStart.south west) -- 
						       		([xshift=.5, yshift=1.5cm] #1SizeStart.south west) -- 
								      ([xshift=-.5, yshift=1.5cm] #2SizeEnd.south east) -- 
								      ([xshift=-.5] #2SizeEnd.south east);
	\node[slotRange] at ([yshift=1.5cm] $(#1SizeStart.south west)!0.5!(#2SizeEnd.south east)$) {#3};
}

\newcommand{\sexpRangeBottom}[3]{
	\draw[{Triangle}-{Triangle}] 	([xshift=1] #1.south west) -- 
						       		([xshift=1, yshift=-1.5cm] #1.south west) -- 
								      ([xshift=-1, yshift=-1.5cm] #2.south east) -- 
								      ([xshift=-1] #2.south east);
	\node[slotRange] at ([yshift=-1.5cm] $(#1.south west)!0.5!(#2.south east)$) {#3};
}

\newcommand{\sexpPointerDown}[3]{
	\draw[-{Triangle}] (#1Type.south)  -- ++(0,-#3) -| ([xshift=.5] #2.north west);
}

\newcommand{\sexpLabelLeft}[5]{
	\node[slotLabel, anchor=south, minimum width=3cm] (#1) at (#2) {#5};
	\draw[-{Triangle}] (#1.east) -- ++(#3, 0) -| ([xshift=.5] #4.north);
}

\newcommand{\sexpLabelRight}[5]{
	\node[slotLabel, anchor=south, minimum width=3cm] (#1) at (#2) {#5};
	\draw[-{Triangle}] (#1.west) -- ++(-#3, 0) -| ([xshift=.5] #4.north);
}

\tikzstyle{slot} = [
	rectangle,
    text centered,
    thick,     
    draw=white,
    fill=black, 
	text=white,
    font=\Large\tt
];

\tikzstyle{slotType} = [
	rectangle, 
	text centered,
    draw=black,
    fill=white, 
    font=\tt
];

\tikzstyle{slotRange} = [
	rectangle, 
	text centered,
    draw=black,
    fill=white, 
    font=\tt
];

\tikzstyle{slotSize} = [
    font=\small\tt
];

\tikzstyle{omitted} = [
    text centered,
    thick,     
	text=white,
    font=\Large\tt
];

\tikzstyle{slotLabel} = [
	rectangle, 
	text centered,
    draw=black,
    fill=white, 
    font=\tt
];

\begin{tikzpicture}

\sexpHeader{(0,5)}{};
\nextSlot{car}	{SEXP}{}{}	{};
\nextSlot{cdr}	{SEXP}{}{}	{};
\nextSlot{tag}	{SEXP}{}{}	{52B};

\sexpRange{sxpinfo}{gengcn}{SEXPREC\_HEADER};
\sexpRangeBottom{car}{tag}{u};

\sexpHeaderOmitted{(0,0)}{header};
\nextSlot{car}	{SEXP}{carval}{\color{blue!50} 1}			{};
\nextSlot{cdr}	{SEXP}{cdrval}{\color{red} R\_NilValue}		{};
\nextSlot{tag}	{SEXP}{tagval}{{\color{red} R\_NilValue}}	{52B};

\sexpHeader{(0,-5)}{};
\nextSlot{car}			{R\_xlen\_t}{length}{}		{};
\nextSlot{cdr}			{R\_xlen\_t}{truelength}{}	{};
\nextAlign{align}		     						{};
\nextDataOmitted{data}	         					{44+nB};

\sexpHeaderOmitted{(0,-10)}{header};
\nextSlot{lengthx}		{R\_xlen\_t}{length}{\color{blue!50} 3}				{};
\nextSlot{truelengthx}	{R\_xlen\_t}{truelength}{\color{blue!50} 0}			{};
\nextSlot{real0}		{double}{REAL(~)[0]}{\color{green!50} 1.0d}			{};
\nextSlot{real1}		{double}{REAL(~)[1]}{\color{green!50} 2.0d}			{};
\nextSlot{real2}		{double}{REAL(~)[2]}{\color{green!50} 3.0d}			{68B};

\groupSlots{reals}		{real0}{real2}	{REAL};

\sexpHeaderOmitted{(0,-17)}{header};
\nextSlot{length}		{R\_xlen\_t}{length}{\color{blue!50} 3}				{};
\nextSlot{truelength}	{R\_xlen\_t}{truelength}{\color{blue!50} 0}			{};
\nextHalf{int0}			{int}{\large INTEGER (~)[0]}{\color{green!50} 1}	{};
\nextHalf{int1}			{int}{\large INTEGER (~)[1]}{\color{green!50} 2}	{};
\nextHalf{int2}			{int}{\large INTEGER (~)[2]}{\color{green!50} 3}	{56B};

\sexpPointerDown{lengthx}{int0}{2.2};
\sexpPointerDown{truelengthx}{int1}{1.7};
\sexpPointerDown{real0}{int2}{1.2};
\sexpPointerDown{real2}{int2}{1.2};

\sexpLabelLeft{lab}{-2,-14.75}{1.5cm}{length}{XLENGTH(~)};
\sexpLabelRight{lab}{21,-14.75}{1.5cm}{int2}{INTEGER(~)[2]};

\end{tikzpicture}